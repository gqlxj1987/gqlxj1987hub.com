title: Golang Summaries
date: 2017-02-23 00:30:28
categories: 杂记
tags: [杂记]
---

[原文链接](http://www.tapirgames.com/blog/golang-summaries)


* 各个类型的value部分，进行copy所需要的字节数


* 关于各个类型部分内置function部分的


* ways to concatenate strings

	*  \+  少于6个strings推荐使用
	
	* strings.Join方法

	* fmt.Sprintf以及fmt.Sprint

	* Buffer方法？


* ways to block current goroutine ‎forever

	* `select{}`

	* `<-chan struct{}(nil)`

	* send value to a channel which no ones will receive values from

	`make(chan struct{}) <- struct{}{}`

	* receive from a channel which no values will be sent to

	`<-make(chan struct{})`
	
	
<!--more-->

* Expressions which can return an optional additional bool return value

	包括三类

	* map

	* channel

	* type assertion


* Types which don't support comparisons

	* map
	
	* slice
	
	* function
	
	* struct types containing incomparable fields
	
	* array types with incomparable elements



* Values that can taken addresses

	* variables
	
	* fields of addressable structs
	
	* elements of addressable arrays
	
	* elements of any slices (whether the slices are addressable or not)
	
	* pointer dereference operations 


* Values that can't taken addresses

	* bytes in strings
	
	* map elements(?)
	
	* dynamic values of interface values (exposed by type assertions)
	
	* constant values
	
	* literal values
	
	* package level functions (这个试验一下)
	
	* methods (used as function values)
	
	* intermediate values

* Types which zero values can be represented with nil

	这里列举一些`nil`所需要的字节数
	
	
	|Type (T)| Size Of T(nil)|
	|:----|----:|
	|pointer|1 word|
	|slice|	3 words|
	|map|1 word|
	|channel|1 word|
	|function|1 word|
	|interface|	2 words|



