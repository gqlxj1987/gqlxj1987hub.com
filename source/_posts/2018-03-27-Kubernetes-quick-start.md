title: Kubernetes 简介
date: 2018-03-27 16:33:48
categories: Kubernetes
tags: [Kubernetes]
---



[原文链接](https://blog.sourcerer.io/a-kubernetes-quick-start-for-people-who-know-just-enough-about-docker-to-get-by-71c5933b4633)



> Kubernetes is a portable, extensible open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation.



掌握一些 Kubernetes 的核心概念：

- Cluster: 一个容器网络，它们之间可以互相通信。
- Nodes: Cluster 里面跑的的节点。
- Master：可以控制其他节点，所有操作命令跑在 master 上，Master 决定 Cluster 里面的哪个节点跑应用。
- Network: Kubernetes 假设你搞好了网络可以让节点互相通信。有很多开源方案可以选择。
- Kubernetes API: Master 开放出来的接口
- Kubelet: 每个 Node 上跑着 Kubelet，负责管理当前 Node，以及和 Master 通信。通信使用 Kubernetes API。
- kubectl: 一个命令行工具，调用 Kubernetes API 和 Master 通信用来管理。
- kubeadm: 一个用来初始化 cluster 的程序，能给 Cluster 配置好所需要的 add-ons。
- etcd: 一个分布式 key-value 数据仓库。存储了所有 Cluster Nodes 的信息。
- Kubernetes objects: 是 cluster 中操作的基本对象，也是会在 Kubernetes 中存储的数据。这些数据用来描述 Cluster 应该处于的状态。包括 deployments, replica sets, services, pods, etc.
- Pod: 一组容器(比如 docker containers)以及它们之间的关系；它们之间有共享的网络和存储。在网络上，这组容器可以通过一个独立 IP 地址访问。
- Service: 定义了 Pods 的逻辑分组 以及如何访问它们。因为 Pod 很容易被创建和销毁，Kubernetes 引入了 Service，只要给 Pods 给一个标签，所以流量都会由标签指定的 Service 处理。
- Deployment: 设定预期状态，如果不符合，Kubernetes 会保证 Cluster 达到这个状态。Deployment 告诉了 Kubernetes 如何创建和更新应用的实例。



