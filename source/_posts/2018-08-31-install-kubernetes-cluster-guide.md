title: Install Kubernetes Cluster guide
date: 2018-08-31 10:33:17
categories: kubernetes

tags: [kubernetes]
---



[原文链接](https://github.com/opsnull/follow-me-install-kubernetes-cluster/tree/v1.8.14)



关于一些机器的优化部分

```shell
cat > kubernetes.conf <<EOF
net.bridge.bridge-nf-call-iptables=1
net.bridge.bridge-nf-call-ip6tables=1
net.ipv4.ip_forward=1
vm.swappiness=0
vm.overcommit_memory=1
vm.panic_on_oom=0
fs.inotify.max_user_watches=89100
fs.file-max=52706963
fs.nr_open=52706963
net.ipv6.conf.all.disable_ipv6=1
net.netfilter.nf_conntrack_max=2310720
EOF
sudo cp kubernetes.conf  /etc/sysctl.d/kubernetes.conf
sudo sysctl -p /etc/sysctl.d/kubernetes.conf
```



设置系统时区部分

```shell
# 调整系统 TimeZone
sudo timedatectl set-timezone Asia/Shanghai

# 将当前的 UTC 时间写入硬件时钟
sudo timedatectl set-local-rtc 0

# 重启依赖于系统时间的服务
sudo systemctl restart rsyslog 
sudo systemctl restart crond
```

