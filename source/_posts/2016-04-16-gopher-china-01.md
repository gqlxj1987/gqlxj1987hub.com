title: Gopher china见闻
date: 2016-04-16 22:58:19
categories: gopher
tags: [golang]
---

第一次参加gopher china，居然还有情侣来的。。不过整体的质量还是挺高的。。

* go与人工智能，主要是讲的go的几个库，包括wukong,zerg,sego,aha，主要是做信息系统的收集以及处理，还有mlf的机器学习的框架。

	这里看到的是，通过机器学习的方式去分析模型，去预测点击？aha的一种展现方式，其实类似于es，是不是可以将es转化为wukong，来可定制我们的数据展现，通过多tag的方式，多维度去展现？
	
	然后，zerg，这个集群爬虫框架，貌似要比python scrapy的爬取要更快，是不是可以在后面的爬取过程中来使用它？
	
	中间有提到用golang来实现spark部分的，这块是不是可以用golang来替换scala？有提到spark的单机迁移
	

* pingcap部分，主要是讲的tidb部分，在mysql client以及db中间，插入一层，将db换做更有效率的tikv，即kv存储

	这块，据说是google spanner的论文，主要是讲通过proxy层部分，实现sql部分的兼容，以及mpp framwork,同时，支持动态扩展，动态紧缩，下层的tikv仿照的是hbase（也是kv？）?
	

* bfe这块，就没怎么涉及，应该主要是gc部分的使用

* bilibili部分，这里讲一个小文件系统，bfs，采用小文件聚合成为一个32g的大文件，来写入到磁盘中。

	整体上，由几个模块组成，volumn模块，store模块，ops模块，meta模块。主要注意的几点：读的时候，主动关闭了预读策略，减少了内存开销，因为读完之后，相关内容会回流到cdn上；块+offset来确定，读写的位置，由于offset设定为8位，所以整体上采用32G的文件作为一个chunk；专门用ops模块来处理迁移，merge，失败后恢复等问题；


*  google 部分，主要是对于x/text/部分的讲解，主要涉及到i18n部分


* mijia，这部分主要是比较相关的web application。

	其实他的经历跟自己还是挺像的，从之前的j2ee转向python,flask，然后再写了scala on play，最后转向了go部分。其中有一点提的挺有道理的，拒绝mgic，至少要找一种best practise，放弃scala的原因就是magic太多。。
	
	学习go，需要了解他的go proverb，例如orm部分，一般情况下的orm是通过reflect来实现，但reflect是不clear的，所以相关reflect的orm，我们就不用，保证符合clear这样的proverb，这也算一种best practise吧。
	
	还有一点，是modelq，他是仿照scala slick的方式，利用go genernate来产生相关的orm部分的代码，
	
	然后，其实挺令我困惑的一点是，一直在做业务部分，从哪里去找到相关动力和激情？
	
* coreos部分，主要是是关于go performs部分的调优，采用go profile来进行相关性能监测，同时还有web的方式的来查看相关的call stack以及time used

	可以尝试在prerelease环境里，对于相关的api进行性能的监控，看下是不是有所优化的空间。。

* go mobile部分  (tomasen)

	最新的go，提供go mobile的方式，为前端提供跨平台的支持，android部分提供aar部分，ios提供framework的注入。
	
	这样的跨平台，优点是，利用go的网络特点，将相关的网络请求封装起来，例如后台下载等，统一android和ios的下载框架等。。带来的特点是快速进行原型开发
	
	缺点部分：不可避免会将go的runtime env带到手机的内存里，这块有风险；一个项目中只能引入一个go mobile生成的包，无法像jar包那样引用多个，相当的不方便；go同java之间的通讯还是依靠类似rpc的方式，效率上需考虑（但好像一般的应用无需考虑这个问题）。。


	
	
	
	


