title: Ko fast k8s deployment
date: 2019-01-03 23:13:40
categories: kubernetes

tags: [kubernetes]
---



[原文链接](https://medium.com/knative/ko-fast-kubernetes-microservice-development-in-go-f94a934a7240)



skaffold tools to wrap process



![skaffold process](https://cdn-images-1.medium.com/max/1600/1*1sQUwU9ZHqeFHXhZpeR98g.png)

`ko` **takes a different approach that leans into Go idioms to eliminate configuration**



```yaml
# This example is based on:
# https://github.com/google/go-containerregistry/blob/master/cmd/ko/test/test.yaml
apiVersion: v1
kind: Pod
metadata:
  name: kodata
spec:
  containers:
  - name: test
    # ko builds and publishes this Go binary, and replaces this
    # with an image name.
    image: github.com/google/go-containerregistry/cmd/ko/test
  restartPolicy: Never
```

由ko来进行后面的部分

![ko process](https://cdn-images-1.medium.com/max/1600/1*2x4bOnBJIhwyDEaS4a7eJA.png)

You only write Kubernetes yamls and code. No Dockerfiles, no Makefiles. You run one command and your latest code is running

```shell
$ko apply -f cmd/ko/test/test.yaml 
```











