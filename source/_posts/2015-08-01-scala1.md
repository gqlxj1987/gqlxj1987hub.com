title: scala学习第三天
date: 2015-08-01 13:27:09
categories: scala
tags: scala
---

<!--more-->

### 控制抽象

函数值用做参数

```scala
def filesMatching(query: String,
	matcher: (String, String) => Boolean) = {
for (file <- filesHere; if matcher(file.getName, query))
	yield file
}
```
闭包，自由变量？

List的高阶函数exists 配合占位符

```scala
def containsOdd(nums: List[Int]) = nums.exists(_ % 2 == 1)
```

Curry化

curry化函数被应用于多个参数列表，

```scala
def first(x: Int) = (y: Int) => x+y
```

```scala
def withPrintWriter(file: File, op: PrintWriter => Unit) {
	val writer = new PrintWriter(file)
	try{
		op(writer)
	} finally {
		writer.close()
	}
}

withPrintWriter(
	new File('date.txt'),
	writer => writer.println(new java.util.Date)
)
```

使用大括号代替小括号包围参数列表，实现内建控制结构

叫名函数


### 组合和继承

elem(s:String): Element

抽象类，无法被实例化，

无参数方法，且仅通过读含有对象的方式访问可变状态

字段值在类初始化的时候被预计算，而方法调用在每次调用的时候都要计算。

scala定义不带参数也没有副作用的方法为无参数方法，省略空的括号，是鼓励的风格，另一方面，永远不要定义没有括号的带副作用的方法。

子类和超类

scala仅微定义准备了两个命令空间，值（字段，方法，包还有单例对象），类型（类和特质名）

多态和动态绑定

使用组合与继承

for循环中的until 
```scala
for(i <- 0 until this.contents.length)
```

### scala的层级

42为Int的实例，不能使用new Int

java中基本类型和引用类型的区别，

Any <- AnyVal, AnyRef, ScalaObject <- ...

AnyRef定义了eq方法，它不能被重写，并且实现为引用相等。 通过eq 引用的相等比较它们的实例部分？那就是==代表为实例的相等？

scala.Null: 每个引用类型的子类，不兼容值类型

Scala.Nothing： 任何其他类型的子类，

### 特质 trait

混入"特质"，而不是继承它们

```scala
trait a {
	def b = {
		
	}
}
```

with 超类中混入特质，

类似带有方法的java接口？

特质不能有"类"参数，构造函数不允许有参数，

class extends trait 构造的时候，需要指明？

```scala
class R(val topLeft: Point, val rightLeft: Ponit) extends  
```

Ordered 用来compare 类似于comparator？

可堆叠改变特质？

更类似与切面，但切面也可继承超类以及相关？

with的次序很重要，一般，越靠近右侧，越先起作用，

多重继承？trait extends超类，使用super部分，线性化解释super的方式？



### 包和引用

```scala
import x.x
import x._
```

```scala
def showFruit(fruit: Fruit) {
	import fruit._
	println(name + "a " + color)
}
```

```scala
import Fruits.{Apple => a, Orange}
```

java允许外部类访问其内部类的私有成员

伴生对象？


### 样式类和模式匹配

case class 和 pattern matching

选择器 match {备选项}

match同switch的比较:

* match始终以值为结果
* 备选表达式永远不会掉到下一个case
* 如果没有模式匹配，会抛出matchError

```case _ =>``` 通配  

模式的种类:

* 通配模式
* 常量模式
* 变量模式
* 构造器模式
* 元组模式
* 类型模式
* 变量绑定

模式防卫

模式重叠

模式部分，用来进行消息的处理？









