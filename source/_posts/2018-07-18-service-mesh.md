title: Service Mesh
date: 2018-07-18 16:17:50
categories: microSerivice
tags: [microService]
---



[原文链接](https://mp.weixin.qq.com/s?__biz=MzIzNjUxMzk2NQ==&mid=2247489530&idx=1&sn=44d76d4161a1754e4f1a7564da643d22&chksm=e8d7e838dfa0612ed595c373bb321014d5f29dc7ae886328d9b88f28ed67dc2b89798f7358bc&scene=27#wechat_redirect)



![nginx mesh](https://cdn-1.wp.nginx.com/wp-content/uploads/2018/04/dia-OG-2018-03-30-nginMesh-Istio-blog-diagram-1024x785-04_1024x785.png)

service mesh 就是将一些公用的部分，抽象出来，作为control plane部分

Service Mesh可以定义为在微服务体系结构中处理服务间通信的基础结构层，它减少了与微服务体系结构相关的复杂性，并提供了许多治理功能，如 -  

* 负载均衡（Load Balancing） 
* 服务发现（Service Discovery） 
* 健康检查（Health Check）
* 身份验证（Authentication） 
* 流量管理和路由（Traffic Management & Routing） 
* 断路和故障转移（Circuit Breaking and Failover Policy） 
* 安全（Security） 
* 监控（Metrics & Telemetry） 
* 故障注入（Fault Injection）



![Sofa Mesh](https://ws1.sinaimg.cn/large/006tKfTcgy1ft75ot24lzj31ec18479s.jpg)

sidecar的场景，

应用容器与日志同步工具在同一个Pod下，共享存储卷，应用程序生成的日志文件会由日志同步工具收集并发送到类似kafka，elasticsearch这样服务中。

在这样的架构下我们获得了什么呢？

- 以容器作为基础打包单元，那么就可以分给不同的团队进行开发测试
- Sidecar容器可重用，可以与不同的容器结合
- 以容器作为错误边界，使服务能够独立开发和测试，比如应用服务在没有日志保存功能的情况下也可以独立运行
- 独立回滚与更新（但需要考虑复杂的版本组合，建议使用语义版本管理对版本进行控制）



![CSE Mesher架构](https://ws1.sinaimg.cn/large/00704eQkgy1frtd3kuwbhj30s50dfjx9.jpg)





[设计商业service mesh](http://www.servicemesher.com/blog/the-desigin-patterns-for-a-commercial-service-mesh/)



