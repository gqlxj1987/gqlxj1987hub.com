title: Devops tips
date: 2018-05-04 17:22:09
categories: devops
tags: [devops]
---



[原文链接](https://mp.weixin.qq.com/s?__biz=MzIwMzg1ODcwMw%3D%3D&idx=1&mid=2247487627&sn=34d437a1253bdf61857cf2ac513285ac&utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_125)



#### 单服务器组发布

手动发布，手动停掉-> 金丝雀发布，先发一组，做流量测试

滚动式发布

1. 滚动式发布一般先发 1 台，或者一个小比例，如 2% 服务器，主要做流量验证用，类似金丝雀 (Canary) 测试。
2. 滚动式发布需要比较复杂的发布工具和智能 LB，支持平滑的版本替换和流量拉入拉出。
3. 每次发布时，先将老版本 V1 流量从 LB 上摘除，然后清除老版本，发新版本 V2，再将 LB 流量接入新版本。这样可以尽量保证用户体验不受影响。
4. 一次滚动式发布一般由若干个发布批次组成，每批的数量一般是可以配置的（可以通过发布模板定义）。例如第一批 1 台（金丝雀），第二批 10%，第三批 50%，第四批 100%。每个批次之间留观察间隔，通过手工验证或监控反馈确保没有问题再发下一批次，所以总体上滚动式发布过程是比较缓慢的 (其中金丝雀的时间一般会比后续批次更长，比如金丝雀 10 分钟，后续间隔 2 分钟)。
5. 回退是发布的逆过程，将新版本流量从 LB 上摘除，清除新版本，发老版本，再将 LB 流量接入老版本。和发布过程一样，回退过程一般也比较慢的。
6. 滚动式发布国外术语通常叫 Rolling Update Deployment。





#### 双服务器组发布

为一次发布分配两组服务器，一组运行现有的 V1 老版本，一组运行待上线的 V2 新版本，再通过 LB 切换流量方式完成发布，这就是所谓的双服务器组发布方式。



蓝绿发布



金丝雀发布



滚动发布





#### 其他发布以及测试



影子测试

![影子](https://mmbiz.qpic.cn/mmbiz_png/UicsouxJOkBdpqMAJvdAY6GFrP17hbic5SnxNNkF4ZjG97KxRkH9cHhHwVVvOB41KDVQKZVjOCJjUcYpzmDySYfw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1)





1. 目标实现老的 legacy 服务迁移升级到新的 experimental 服务。
2. 测试开始前，需要在测试环境部署一份 legacy 服务和 experimental 服务，同时将生产数据库复制两份到测试环境。同时需要将生产请求日志收集起来，一般可以通过 kafka 队列收集，然后通过类似 goreplay附录 6.8这样的工具，消费 kafka 里头的请求日志，复制回放，将请求分发到 legacy 服务和 experimental 服务，收到响应后进行比对，如果所有响应比对成功，则可以认为 legacy 服务和 experimental 服务在功能逻辑上是等价的；如果有响应比对失败，则认为两者在功能逻辑上不等价，需要修复 experimental 并重新进行影子测试，直到全部比对成功。根据系统复杂度和关键性不同，比对测试时间短的可能需要几周，长的可达半年之久。
3. 影子测试因为旁路在独立测试环境中进行，可以对生产流量完全无影响。
4. 影子测试一般适用于遗留系统的等价重构迁移，例如.net 转 Java，或者 SQLServer 数据库升级为 MySQL 数据库，且外部依赖不能太多，否则需要开发很多 mock，测试部署成本会很高，且比对测试更加复杂和不稳定。





#### 比较



![比较](https://mmbiz.qpic.cn/mmbiz_png/UicsouxJOkBdpqMAJvdAY6GFrP17hbic5SAq6JtYTCQXeFC6QKUibiclw66TyQFiavQxSHbAl4ZzxTAE3bxW2pkDHhQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1)



