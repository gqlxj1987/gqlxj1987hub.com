title: Kubernetes Best Practices Tips
date: 2018-09-09 22:28:26
categories: k8s

tags: [k8s]
---



[视频地址](https://www.youtube.com/watch?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_140&v=BznjDNxp4Hs)

主要是介绍weave cloud中k8s的一些实践部分

## building containers



clair, 对镜像的静态分析

#### use the build pattern

build container -> build artifact -> runtime container

在镜像中，尽量使用非root用户？yaml中有相应的选项



#### One process per container

pod运行所有的process

#### Don't restart on failure. Crash cleanly instead

举的例子就是PM2运行nodejs程序，k8s会重启container，只需要返回更高的code让k8s去检测到

#### log to stdout and stderr



## Deployments



#### 记录每次deploy的record部分

#### 关于label的使用

#### Use sidecar containers for proxies, watchers, etc

程序只需要跟本地的端口部分打交道，由proxy去同其他部分，进行auth,rate,limiting等等相关的交互

#### Don't use sidecars for bootstarpping. Just use initial container instead

#### Don't use :latest or no tag



#### Health check

* Readiness->是否已经启动，并接收流量
* Liveness->是否还在运行



## Services

#### type: NodePort can be "good enough"

### map external services to internal ones

内部一个点，外部一个点，从而避免内外部混用，配置文件部分也不同

#### Use Helm Charts

#### Just assume that all your downstream dependencies are unreliable

#### Use a "Service Mesh"

istio 或者linkerd部分？可以无缝接到你的container部分

#### Use Namespaces to split up your cluster

#### Unleash the chaos monkey

?





## Challenges on K8s Cluster

### Infrastructure的版本控制



Terraform to configure infrastructure



Kubediff



### Continuous Delivery

branch and commit Id in the image tag



[一个工具flux](https://github.com/weaveworks/flux)



###关于报警和监控部分

使用k8s的服务发现，tsdb部分的存储，pull vs push



