title: 架构deep dive
date: 2018-11-25 14:15:32
categories: architecture

tags: [architecture]
---



主要是针对ee的架构的deep dive部分

一共4个概念，PSC，PWS，PAS，PMS



Storage center部分，存储中心，包含两个部分，Meta Server和Job Server

Meta Server主要是解决异构数据融合困难的问题，通过统一的Meta Interface，实现异构多数据源之间的schema的统一

Job Server统一管理在线和离线任务，支持多分布式引擎



Application Server是AI在线服务的编排和治理系统，提供异构运行在线应用程序环境，内嵌AI服务中间件（Predictor、RtiDB、自学习、批量预估服务、Notebook）





![image-20181125144205684](/iamges/QQ20181125-0.png)





###PWS部分

主要是调度的问题

* HOL问题 vs 饿死问题
* 资源抢占问题
* 调度系统数据指标



关于yarn部分调度以及k8s的调度之上有什么区别？

