title: Clean Code
date: 2018-06-11 17:30:47
categories: code
tags: [code]
---



[原文链接](https://gaston.life/books/clean-coder/)



不止是code的一些clean，更是对自己一些原则的clean以及保持



## Self Improvement

Plan on working 60 hours a week: 40 hours for your employer and 20 hours for you.



The minimum things you must know are:

- Design patterns: GOF and POSA books.
- Design principles: SOLID and component principles.
- Methods: XP, Scrum, Lean, Kanban, Waterfall, Structured Analysis, and Structured Design.
- Disciplines: TDD, OOD, Structured Programming, CI/CD, and Pair Programming
- Artifacts: UML, DFDs, Structure Charts, Petri Nets, State Transition Diagrams and Tables, Flow Charts, and Decision Tables.

Learn things outside of your comfort zone. Learn Prolog and Forth.

Do a simple **10-minute programming exercise** in the morning, to warm up, and in the afternoon, to cool down.



