title: Incomplete Guide for Kubernetes
date: 2018-07-23 11:04:54
categories: k8s
tags: [k8s]
---



[原文链接](https://vinta.ws/code/the-complete-guide-to-google-kubernetes-engine-gke.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_135)



## Concepts

Nodes:

- Cluster: A set of machines, called nodes, that run containerized applications.
- Node: A single virtual or physical machine that provides hardware resources.
- Edge Node: The node which is exposed to the Internet.
- Master Node: The node which is responsible for managing the whole cluster.



Objects:

- Pod: A group of one or more tightly related containers. Each pod is like a logical host has its own IP and hostname.
- PodPreset: A set of pre-defined configurations can be injected into Pods automatically.
- Service: A load balancer of a set of Pods which selected by labels, also called Service Discovery.
- Ingress: A revered proxy acts as an entry point to the cluster, which allows domain-based and path-based routing to different Services.
- ConfigMap: Key-value configuration data can be mounted into containers or consumed as environment variables.
- Secret: Similar to ConfigMap but for storing sensitive data only.
- Volume: A ephemeral file system whose lifetime is the same as the Pod.
- PersistentVolume: A persistent file system that can be mounted to the cluster, without being associated with any particular node.
- PersistentVolumeClaim: A binding between a Pod and a PersistentVolume.
- StorageClass: A storage provisioner which allows users request storages dynamically.
- Namespace: The way to partition a single cluster into multiple virtual groups.



Controllers:

- ReplicationController: Ensures that a specified number of Pods are always running.
- ReplicaSet: The next-generation ReplicationController.
- Deployment: The recommended way to deploy stateless Pods.
- StatefulSet: Similar to Deployment but provides guarantees about the ordering and unique names of Pods.
- DaemonSet: Ensures a copy of a Pod is running on every node.
- Job: Creates Pods that runs to completion (exit with 0).
- CronJob: A Job which can run at a specific time or run regularly.
- HorizontalPodAutoscaler: Automatically scales the number of Pods based on CPU and memory utilization or custom metric targets.

 



Pods have following life cycles (states):

- Pending
- Running
- Succeeded
- Failed
- Unknown



ConfigMap的作用？



关于ingress部分

Pods in Kubernetes are not reachable from outside the cluster, so you need a way to expose your Pods to the Internet. Even though you could associate Pods with a Service of the right type, i.e., `NodePort` or `LoadBalancer`, the recommended way to expose services is using Ingress. You can do a lot of different things with an Ingress, and there are many types of Ingress controllers that have different capabilities.

There are some reasons to choose Ingress over Service:

- Service is internal load balancers and Ingress is a gateway of external access to Services
- Service is L3 load balancer and Ingress is L7 load balancer
- Ingress allows domain-based and path-based routing to different Services
- It is not efficent to create a cloud provider's load balancer for each Service you want to expose











