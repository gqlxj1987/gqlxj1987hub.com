title: Google Spanner
date: 2017-02-15 23:28:51
categories: spanner
tags: [spanner]
---

[Spanner: Google’s Globally-Distributed Database](https://research.google.com/archive/spanner.html)

## Agenda:

* Overview

* Property

* Implements



### Spanner

#### Spanner的优势

分布式多版本的数据库

* 支持ACID

* 类sql query

* Schematized table

* 半关系型


这里好像提到NewSql，相对于关系型以及NoSql



* 针对读事务的无锁机制

* 额外的一致性模型

* 支持副本以及2PC

* 实时性部分？TrueTime，全局time部分？

