title: Pytorch Introduction
date: 2018-01-30 16:06:51
categories: pytorch
tags: [pytorch]
---



[英文链接](http://pytorch.org/2018/01/19/a-year-in.html)



[中文链接](https://medium.com/@drumrick/2017-%E5%9B%9E%E9%A1%A7%E6%96%87%E6%91%98%E8%A6%81%E7%B3%BB%E5%88%97%E4%BA%8C-pytorch-dff8dcb12bc)



我自己是覺得目前 PyTorch 的 ecosystem 還是不如 TensorFlow / Keras 完整，但是在 NLP 相關研究上的使用相對於其他研究或是其他框架是有比較多的趨勢。