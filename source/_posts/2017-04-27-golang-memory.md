title: Golang 内存管理初探
date: 2017-04-27 00:03:01
categories: golang
tags: [golang]
---


[golang内存管理(上篇)](https://mp.weixin.qq.com/s/LXj5rrtmxB7NyQbOg9N5wA)


tcmalloc的介绍

一个Goroutine部分，分为G,M,P三个部分组成

* G Goroutine执行的上下文环境

* M 操作系统线程

* P Processer. 调度器


#### 逃逸分析

```golang
package main
import ()
func foo() *int {  
  var x int
  return &x
}
func bar() int {
  x := new(int)
  *x = 1
  return *x
}
func main() {}
```

关于执行的命令如下：

```shell
$ go run -gcflags '-m -l' escape.go
./escape.go:6: moved to heap: x
./escape.go:7: &x escape to heap
./escape.go:11: bar new(int) does not escape
```

注意上面的代码，foo()的x是分配到堆上，而bar()中的x是分配到栈上




