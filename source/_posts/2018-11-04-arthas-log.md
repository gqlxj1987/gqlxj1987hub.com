title: Arthas Log Bug追查
date: 2018-11-04 17:31:36
categories: java

tags: [java]
---



[原文链接](https://mp.weixin.qq.com/s/boGS0VK45mZ_zT25K44S5Q)



利用arthas直接在线上定位问题的过程，主要使用 `sc`和 `getstatic`命令。

- https://alibaba.github.io/arthas/sc.html
- https://alibaba.github.io/arthas/getstatic.html





例如：

```shell
$ sc io.netty.channel.nio.NioEventLoop
class-info        io.netty.channel.nio.NioEventLoop
 code-source       file:/opt/app/plugins/tair-plugin/lib/netty-all-4.0.35.Final.jar!/
 name              io.netty.channel.nio.NioEventLoop
 isInterface       false
 isAnnotation      false
 isEnum            false
 isAnonymousClass  false
 isArray           false
 isLocalClass      false
 isMemberClass     false
 isPrimitive       false
 isSynthetic       false
 simple-name       NioEventLoop
 modifier          final,public
 annotation
 interfaces
 
super-class     +-io.netty.channel.SingleThreadEventLoop
                     +-io.netty.util.concurrent.SingleThreadEventExecutor
                       +-io.netty.util.concurrent.AbstractScheduledEventExecutor
                         +-io.netty.util.concurrent.AbstractEventExecutor
                           +-java.util.concurrent.AbstractExecutorService
                             +-java.lang.Object
 
class-loader      +-tair-plugin's ModuleClassLoader
 classLoaderHash   73ad2d6

```



通过查看源码，了解相关的logger的实现

```shell
$ getstatic -c 73ad2d6 io.netty.channel.nio.NioEventLoop logger 'getClass().getName()'
field: logger
@String[io.netty.util.internal.logging.Slf4JLogger]
```



**tair插件里的logback没有设置ROOT logger，所以它的默认level是DEBUG，并且默认的appender会输出到stdout里。**



一些经验

* logback默认的ROOT logger level是 `DEBUG`，输出是stdout
* 利用arthas的 `sc`命令定位具体的类
* 利用arthas的 `getstatic`获取static filed的值
* 利用logger parent层联的特性，可以向上一层层获取到ROOT logger的配置



