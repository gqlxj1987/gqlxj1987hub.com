title: Storytelling in Dashboard
date: 2018-04-23 11:45:37
categories: dashboard
tags: [dashboard]
---



[原文链接](http://susielu.com/data-viz/storytelling-in-dashboards?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_124)



A common categorization for data visualizations is **exploratory** and **explanatory**.

数据可视化的关键是可解释，可讲故事



[图解机器学习](http://www.r2d3.us/%E5%9B%BE%E8%A7%A3%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0/)

关于决策树部分，决策树的末节又叫做叶节点，预测取决于叶节点内哪一类的样本相对较多



Commonly we reduce these two modes into:
*Explanatory* = has a story e.g. scrollytelling
*Exploratory* = doesn't have a story e.g. dashboards





