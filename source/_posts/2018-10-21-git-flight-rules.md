title: Git Flight Rules
date: 2018-10-21 23:28:36
categories: git

tags: [git]
---



[repo地址](https://github.com/k88hudson/git-flight-rules)



提交的信息写错了

```shell
$git commit --amend --only -m 'xxxxxxx'
```

### 我想要暂存一个新文件的一部分，而不是这个文件的全部

一般来说, 如果你想暂存一个文件的一部分, 你可这样做:

```
$ git add --patch filename.x
```

`-p` 简写。这会打开交互模式， 你将能够用 `s` 选项来分隔提交(commit)； 然而, 如果这个文件是新的, 会没有这个选择， 添加一个新文件时, 这样做:

```
$ git add -N filename.x
```

然后, 你需要用 `e` 选项来手动选择需要添加的行，执行 `git diff --cached` 将会显示哪些行暂存了哪些行只是保存在本地了。







[更多的tips](https://github.com/git-tips/tips)



