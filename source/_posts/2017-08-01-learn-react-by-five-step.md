title: Learn React by five step
date: 2017-08-01 07:43:52
categories: react
tags: [react]
---

[How to Learn React: A Five-Step Plan](https://www.lullabot.com/articles/how-to-learn-react)




### Step One - React Documentation + Code Sandbox

[Code Sandbox](https://codesandbox.io/s/new)

Key Takeaways

* Introducing JSX
* Rendering Elements
* Components and Props
* State and Lifecycle (super important!)
* Handling Events
* Composition vs Inheritance
* Thinking In React




### Step Two - React Fundamentals Course

[Fundamentals Course](https://reacttraining.com/online/react-fundamentals)

[other resource](https://egghead.io/courses/react-fundamentals), [other2](https://www.youtube.com/playlist?list=PLoYCgNOIyGABj2GQSlDRjgvXtqfDxKm5b)

Key Takeaways

* Re-enforcement of principles from React docs
* Intro to the build tools for React projects, particularly webpack
* Stateless functional components
* Routing (how you navigate from one “page” to the next)
* Fetching async data




### Step Three - Read ReactBits

[ReactBits](https://vasanthk.gitbooks.io/react-bits/)

Key Takeaways

* Design Patterns and Techniques (most important tip is probably [this](https://vasanthk.gitbooks.io/react-bits/patterns/27.passing-function-to-setState.html))

* Anti-Patterns
* Perf Tips


Facebook came up with an answer to these questions—the [Flux architecture](https://facebook.github.io/flux/). Some time later, Dan Abramov created an implementation of Flux he called [Redux](http://redux.js.org/).


[MobX](https://mobx.js.org/) is recommend



### Step Four - Redux Documentation + Redux Video Series

[Getting Started with Redux](https://egghead.io/courses/getting-started-with-redux)

Key Takeaways

* Three principles of Redux

* Actions
* Reducers
* Data flow
* Usage with React
* Async actions
* Async Flow
* Middleware




### Step Five - The Complete Redux Book + Redux Video Series Part 2

[Redux Book](https://leanpub.com/redux-book)

Key Takeaways

* Re-enforcement of principles from Redux docs and/or first video series

* Understanding of basic functional programming principles
* Understanding of creating and writing Redux middleware
* Understanding of how to architect a React + Redux application




### Other things

[Redux Saga](https://github.com/redux-saga/redux-saga) middlemare for Redux

[Reselect](https://github.com/reactjs/reselect)







 