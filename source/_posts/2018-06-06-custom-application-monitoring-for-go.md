title: custom application monitoring for go
date: 2018-06-06 17:01:25
categories: golang
tags: [golang]
---



[原文链接](https://www.logicmonitor.com/blog/custom-application-monitoring-go/)



our TSDB backup system has several components that need to be monitored, including a metadata database, external storage, backup agents, and a centralized backup scheduler, along with with performance metrics such as network throughput, CPU utilization, memory utilization, and disk performance.



expose application metric

```go
import (
 "encoding/json"
 "log"
 "net/http"
 "runtime"
)

func Performance(w http.ResponseWriter, req *http.Request) {
 results := make(map[string]float32)

 // get number of Goroutines
 // https://golang.org/pkg/runtime/#NumGoroutine 
 numRoutines := runtime.NumGoroutine()
 results["GoRoutines"] = float32(numRoutines)

 // get memory stats
 // https://golang.org/pkg/runtime/#MemStats 
 var memStats runtime.MemStats
 runtime.ReadMemStats(&memStats)

 // bytes allocated and not yet freed
 results["MemAlloc"] = float32(memStats.Alloc) 

 // number of frees
 results["MemFrees"] = float32(memStats.Frees) 
 
 // bytes allocated and not yet freed
 results["MemHeapAlloc"] = float32(memStats.HeapAlloc) 
 
 // bytes in idle spans
 results["MemHeapIdle"] = float32(memStats.HeapIdle) 

 // bytes in non-idle span
 results["MemHeapInUse"] = float32(memStats.HeapInuse) 

 // total number of allocated objects
 results["MemHeapObjects"] = float32(memStats.HeapObjects) 

 // bytes obtained from system
 results["MemHeapSys"] = float32(memStats.HeapSys) 

 // number of mallocs
 results["MemMallocs"] = float32(memStats.Mallocs) 

 // total number of garbage collections
 results["MemNumGc"] = float32(memStats.NumGC) 

 //total time that the garbage collector has paused the program
 results["MemPauseTotalNs"] = float32(memStats.PauseTotalNs) 

 // bytes obtained from system 
 results["MemSys"] = float32(memStats.Sys)

 resp, err := json.Marshal(results)
 if err != nil {
  log.Printf("error: couldn't marshal queue metrics to json")
  w.WriteHeader(http.StatusInternalServerError)
 } else {
  w.Write(resp)
 }
}
```



将这些收集到的内容，放到TSDB里中，(json格式)