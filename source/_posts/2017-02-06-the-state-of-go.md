title: The state of Go(1.8)
date: 2017-02-06 23:02:37
categories: golang
tags: [golang]
---


Changes since Go 1.7

* The Language

* The Standard Library

* The Runtime

* The Tooling

* The Community



### The Language


* Conversion rules

可以针对same sequence of fields and corresponding fields with same type 进行类型强转



* Ports to other platforms

针对大小端的32位linux系统的兼容性

同时针对arm进行支持


* Tool

针对context部分的修正

```
go tool fix -diff -force=context state-of-go/tools/gofix.go
```


vet部分可以静态检查代码？

```
go vet state-of-go/tools/govet.go
```


SSA everywhere?




### Runtime

* Detection of concurrent map accesses


关于map的并发读写问题，

是指掩盖了map的并发读写的问题，还是检测map的并发读写问题？


* bench use mutex contention profile

关于mutex部分，wide protection and narrow protection



* GC history in tweets

defer and cgo is faster


### The Standard Library


* sort部分

```
sort.Sort(byName(p))
sort.Sort(byAge(p))
```

原生支持按照field部分进行排序，无需定义之前len,less,swap等等操作


同时也只支持sort.Slice


* plugins部分


```
go build -buildmode=plugin
```

可以编译成为.so文件，然后在第三方文件中使用，通过Lookup的方式，来寻找相关的函数以及变量


* http shutdown

加入http.server的shutdown机制

这样能够及时判断http.server是否退出？


* HTTP/2

可以使用http.Response来推送进行？使用http.Pusher的功能？








