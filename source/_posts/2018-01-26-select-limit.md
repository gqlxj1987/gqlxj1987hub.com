title: 毫秒级实时排序
date: 2018-01-26 15:53:26
categories: sort

tags: [sort]
---



[实践：几十亿条数据分布在几十个节点上的毫秒级实时排序方法](https://yq.aliyun.com/articles/378962?spm=a2c4e.11153940.bloghomeflow.105.FrxURS)



`select a from tb order by a limit 100, 10;`



`对分布在几十个节点上的上亿条数据要求1秒内排序。`



