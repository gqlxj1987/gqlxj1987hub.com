title: Consistent Hashing
date: 2018-04-17 10:11:29
categories: hash
tags: [hash]
---



[原文链接](https://medium.com/@dgryski/consistent-hashing-algorithmic-tradeoffs-ef6b8e2fcae8)



### mod-N hashing

This tends to rule out cryptographic ones like [SHA-1](https://en.wikipedia.org/wiki/SHA-1)or [MD5](https://en.wikipedia.org/wiki/MD5). Yes they are well distributed but they are also too expensive to compute — there are much cheaper options available. Something like [MurmurHash](https://en.wikipedia.org/wiki/MurmurHash) is good, but there are slightly better ones out there now. Non-cryptographic hash functions like [xxHash](https://github.com/cespare/xxhash), [MetroHash](https://github.com/dgryski/go-metro) or [SipHash1–3](https://github.com/dgryski/go-sip13) are all good replacements.



```go
server := serverList[hash(key) % N]
```



What are the downsides of this approach? The first is that if you change the number of servers, almost every key will map somewhere else. This is bad.

optimal option

- When adding or removing servers, only 1/nth of the keys should move.
- Don’t move any keys that don’t need to move.



paper and system

* [Consistent Hashing and Random Trees: Distributed Caching Protocols for Relieving Hot Spots on the World Wide Web](https://www.akamai.com/es/es/multimedia/documents/technical-publication/consistent-hashing-and-random-trees-distributed-caching-protocols-for-relieving-hot-spots-on-the-world-wide-web-technical-publication.pdf)
* last.fm’s [Ketama memcached client](https://www.last.fm/user/RJ/journal/2007/04/10/rz_libketama_-_a_consistent_hashing_algo_for_memcache_clients).
* [Dynamo: Amazon’s Highly Available Key-value Store](https://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf)



In practice, each server appears multiple times on the circle. These extra points are called “virtual nodes”, or “vnodes”. This reduces the load variance among servers. With a small number of vnodes, different servers could be assigned wildly different numbers of keys. 引入虚拟节点来管理



One of the other nice things about ring hashing is that the algorithm is straight-forward.

```go
func (m *Map) Add(nodes ...string) {
    for _, n := range nodes {
        for i := 0; i < m.replicas; i++ {
            hash := int(m.hash([]byte(strconv.Itoa(i) + " " + n)))
            m.nodes = append(m.nodes, hash)
            m.hashMap[hash] = n
        }
    }
    sort.Ints(m.nodes)
}
```

each one is hashed `m.replicas`times with slightly different names ( `0 node1`, `1 node1`, `2 node1`, …)

注意这里有replicas的概念，这样子多写几份?然后是对于nodes进行排序，方便进行binary search during lookup

```go
func (m *Map) Get(key string) string {
    hash := int(m.hash([]byte(key)))
    idx := sort.Search(len(m.keys),
        func(i int) bool { return m.keys[i] >= hash }
    )
    if idx == len(m.keys) {
        idx = 0
    }
    return m.hashMap[m.keys[idx]]
}
```



### Ketama

[go-ketama](https://github.com/dgryski/go-ketama)



### Not enough

Ring hashing still has some problems.

* the load distribution across the nodes can still be uneven



### Jump hash

Jump Hash addresses the two disadvantages of ring hashes: 

it has no memory overhead and virtually perfect key distribution.

代码实现: [github.com/dgryski/go-jump](https://github.com/dgryski/go-jump)

```go
func Hash(key uint64, numBuckets int) int32 {
  var b int64 = -1
  var j int64
  for j < int64(numBuckets) {
    b = j
    key = key*2862933555777941757 + 1
    j = int64(float64(b+1) *
      (float64(int64(1)<<31) / float64((key>>33)+1)))
  }
  return int32(b)
}
```

It then uses the random numbers to “jump forward” in the list of buckets until it falls off the end

It’s fast and splits the load evenly. What’s the catch? The main limitation is that it only returns an integer in the range `0..numBuckets-1`. It doesn’t support arbitrary bucket names. (With ring hash, even if two different instances receive their server lists in a different order, the resulting key mapping will still be the same.) 



### Multi-Probe Consistent Hashing



The basic idea is that instead of hashing the nodes multiple times and bloating the memory usage, the nodes are hashed only once but the key is hashed `k`times on lookup and the closest node over all queries is returned. The value of `k` is determined by the desired variance. For a peak-to-mean-ratio of 1.05 (meaning that the most heavily loaded node is at most 5% higher than the average), `k` is 21. With a tricky data structure you can get the total lookup cost from O(k log n) down to just O(k). [My implementation](https://github.com/dgryski/go-mpchash) uses the tricky data structure.



### Maglev Hashing





### Replication

副本机制



### Weighted Hosts



### Load Balancing



### Benchmarks

![benchmarks](https://cdn-images-1.medium.com/max/1600/1*fl7F4cFSXEcFilGt5-NvFw.png)



### conclusion





[参考资料](http://www.evanlin.com/til-consistent-hashing-algorithm-tradeoffs/?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_123)



