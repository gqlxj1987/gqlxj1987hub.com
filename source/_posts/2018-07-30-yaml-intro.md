title: Yaml Intro
date: 2018-07-30 16:54:18
categories: yaml
tags: [yaml]
---



[原文链接](https://arp242.net/weblog/yaml_probably_not_so_great_after_all.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_136)



#### can be hard to edit, for large files

It is difficult to see “where” in the file you are because it may be off the screen. You’ll need to scroll up, but then you need to keep track of the indentation, which can be pretty hard even with indentation guides, especially since 2-space indentation is the norm and [tab indentation is forbidden](http://www.yaml.org/faq.html)[2](https://arp242.net/weblog/yaml_probably_not_so_great_after_all.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_136#fn:2).



主要的几种配置文件格式：

The [YAML spec](http://yaml.org/spec/1.2/spec.pdf) is 23,449 words; for comparison, [TOML](https://github.com/toml-lang/toml) is 3,339 words, [JSON](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf) is 1,969 words, and [XML](https://www.w3.org/TR/REC-xml/) is 20,603 words.



一些关于value的类型的解释

```yaml
python: 3.5.3
postgres: 9.3
```

解释成为

`{'python': '3.5.3', 'postgres': 9.3}`



All of this – and more – is why many experienced YAMLers will often quote all strings, even when it’s not strictly required



不同语言解析上，可能yaml的解析规则上有所不同？

The reason for this is that there are multiple YAML documents in a single file (`---` start the document). In Python there is the `load_all()` function to parse all documents. Ruby’s `load()` just loads the first document, and as near as I can tell, doesn’t have a way to load multiple documents.



#### Conclusion

并不是说yaml不好，只是指明yaml不能够更greater

可以尝试更好的方式，such as [TOML](https://github.com/toml-lang/toml)

