title: Travis CI - Step/Job/Stage
date: 2018-11-01 14:43:45
categories: ci

tags: [ci]
---



[原文链接](https://www.hwchiu.com/travisci-step-job-stage.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_145)



```yaml
language: go

go:
  - "1.8"

before_install:
  - echo "before_install"

install:
  - echo "install"
  - 
before_script:
  - echo "before_script"

script:
  - echo "script"

```



![travisci steps](https://i.imgur.com/Mrb22oE.png)



关于`Job`的定义，`Job` 就是一個歷經 `TravisCI` 生命週期所有步驟的基本單位。



关于`Custom job`的实现

![custom job](https://i.imgur.com/Kr9KDi3.png)



引入`job:include`的概念

```yaml
language: go

go:
  - "1.8"
  
before_install:
  - echo "before_install"
install:
  - echo "install"
before_script:
  - echo "before_script"
script:
  - echo "script"

jobs:
  include:
    - stage: Custom Testing
      name: Unit-Testing
      go: "1.8"
      script: echo "unit script"
    - name: Integration-Testing
      before_install: "Integration-Testing_before_install"
      go: "1.9"
      script: "Integration-Testing_script"

```



`Stage` 的特色以及概念如下

1. 由一群 `Job` 組成
2. 只要有一個 `Job` 失敗，該 `Stage` 就會被視為失敗
3. 只有當該前 Stage 是成功的狀態，才會執行下一個 Stage



![多个stage](https://i.imgur.com/YkGmTQN.png)







