title: WorldCup graphQL
date: 2018-06-26 15:17:11
categories: graphQL
tags: [graphQL]
---



[原文链接](https://medium.freecodecamp.org/building-the-2018-world-cup-graphql-api-fab40ccecb9e)



![sql struct](https://cdn-images-1.medium.com/max/2000/0*zfRcG-bR_cMXR8cF)



```sql
query {
  players(name:”Lionel Messi”) {
    name
    dob
    allGoals
  }
}
```

