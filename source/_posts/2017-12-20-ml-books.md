title: 机器学习资料
date: 2017-12-20 14:54:41
categories: machine learning
tags: [machine learning]
---



借鉴于[d0evi1的博客](http://d0evi1.com/tecbook/)



### 基础篇

数学基础

* [线性代数](http://open.163.com/special/opencourse/daishu.html)
* [统计学](https://book.douban.com/subject/1230154/)， [统计学导论](https://open.163.com/movie/2011/5/M/O/M807PLQMF_M80HQQGMO.html)
* [微积分基础](https://www.coursera.org/learn/calculus1)
* [微积分-积分学](https://open.163.com/movie/2017/5/K/0/MCIM9UCLC_MCIO21CK0.html)



机器学习

* [吴恩达: machine learning－coursera](https://www.coursera.org/learn/machine-learning)
* [吴恩达: 机器学习－斯坦福](http://open.163.com/special/opencourse/machinelearning.html)
* [统计学习方法](http://book.douban.com/subject/10590856/)



coursera的课程

* [机器学习基础](https://class.coursera.org/ntumlone-003/lecture)
* [机器学习技法](https://class.coursera.org/ntumltwo-002/lecture)



交大课程

* [机器学习导论](http://ocw.sjtu.edu.cn/G2S/OCW/cn/CourseDetails.htm?Id=397)
* [统计机器学习](http://ocw.sjtu.edu.cn/G2S/OCW/cn/CourseDetails.htm?Id=398)



### 神经网络



[神经网络简史](http://blog.sina.com.cn/s/blog_71329a960102v1eo.html)

[深度学习 from google](https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63798118150923)



李宏毅[bilili](http://space.bilibili.com/23852932/#!/channel/detail?cid=11583)



coursera课程

[机器学习中使用的神经网络](https://class.coursera.org/neuralnets-2012-001/lecture)



tensorflow:

[standford tensorflow: CS20SI](https://www.youtube.com/watch?v=g-EvyKpZjmQ&list=PLSPPwKHXGS2110rEaNH7amFGmaD5hsObs&spfreload=10)

[配套课件ppt](http://web.stanford.edu/class/cs20si/syllabus.html)





### NLP

语法分析_陆俭明

[CS224D 深度学习与自然语言处理](https://www.youtube.com/watch?v=DJHvaGU9SW8) [ppt](http://cs224d.stanford.edu/lectures/)





### 推荐系统

[Recommender Systems](http://www.slideshare.net/xamat/recommender-systems-machine-learning-summer-school-2014-cmu)



[Recsys 2016 tutorial: Lessons learned from building real-life recommender systems](http://www.slideshare.net/xamat/recsys-2016-tutorial-lessons-learned-from-building-reallife-recommender-systems)





### others

[深度学习与计算机视觉](http://study.163.com/course/introduction.htm?courseId=1003223001#/courseDetail)