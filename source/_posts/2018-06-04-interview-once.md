title: 面试总结
date: 2018-06-04 21:55:41
categories: 面试
tags: [面试]
---



三年来的第一次面试，算是勉勉强强地过了

期间一些问题的回顾

* 简历上关于新美的部分，有些太少，应该多加点，并且侧重于数据以及优化
* 创业公司，一个人干成一个大工程，要强调这部分的一个挑战性，包括技术上的挑战性，沟通上的挑战性，最后成果上的展现的挑战性
* 关于成长性的预期，是从大公司的一个螺丝钉到创业公司的独当一面，强调现有工作的全面性
* 从内部系统转向了外部系统，从并发性以及复杂性上，给出相应的答案
  * 输入法后端的并发性
  * 推送系统的完备性
  * 数据平台的完整性，多维度性
  * 处理框架的实时性上
  * 多机房部署的复杂性，以及运维上的复杂性
* 尽量从数据上给出例子
  * qps，接口成功率
  * 推送系统的成功率，推送的及时性
  * 离线处理平台部分，涉及多日志处理部分，完整性，复杂性
  * 在线处理平台的实时性，框架的选择，结果的呈现
  * 其他一些技术上的选择，以及成果



twitter的fingle框架-> 为什么不用gRPC框架，后续来说。。

技术上的专家部分->广度的专家->深度上的专家



---

继续第二面部分

方向上的一些兴趣点部分？

对于基础上的预习不够

简历上关于语言的写法有问题，导致很多还以为会php。。。还是关于侧重点方面，有点牵强

了解地多，还要了解地深，很容易给别人的感觉就是杂而不精。。

不过，经过这次的面试，愈发觉得谦卑的重要性，不管是大公司还是小公司，不管身处什么职位，任何人都有其平等对待的必要，而不是一种店大欺客的感觉。。。

面试也是一种学习和了解，摆好心态，争取自己在面试中学习到更多。。

同时，算法还是偏弱，还要继续刷leetcode。。。

---

所谓的人情三面，终于体会到了。。。

关于业务和技术方面，最终的方向是什么？还是给什么做什么？

传说中的技术上的专家部分，是否还有机会？

关于kafka的一个整合部分，关于规范producer以及consumer部分，以及对突发流量部分的一个把控部分，还是挺有启发的。。

在面试的时候，不用考虑去迎合面试官，尝试去表达自己的想法，尽量坚定自己的想法，不用随着面试官去摇摆，虽然有各种压力和想法，但面试当做学习的话，也要表现自己的思路，自己优点在哪里。。尽量不要紧张。。

在面试中去寻找自己感兴趣的部分，觉得有用的地方。。

---



第四范式的面试：

* 关于内存池分配问题，多线程部分，每个线程独立一个内存池，缓存的局部性
* 算法题，还算是蒙对了。。



技术经理面的时候，会问一些关于管理风格上的问题，以及自己在遇到一些不如人意的环境下，会如何处理的问题。不过，感觉还是有点没答好，关于未来的方向上，有什么遗憾之类。。



最后面的时候，对岗位的match程度产生了怀疑，不过自己也在介绍的时候，没有说好，只是在说明自己对于底层这块有兴趣，但是实际上的一些经验上，没有表述清楚，没有表达自己对于底层优势的部分？

关于业界的最佳实践部分，并不算最佳实践部分，在对于业务的方向上，去选取对业务更合适的架构，并从这其中发现更多的问题，解决这些问题，包括应用层，以及底层部分，都可以去优化。。



---

freewheel的面试：

* 关于数据的严谨性部分，如丢失率部分，有没有计算？
* api gateway的方式，关于限流部分，有没有更好的方式，除了丢弃之外，通过redis来实现限流器部分，
* 关于服务的测试/上线部分，通过gor replay部分，对相关流量进行还原



关于数据的lazy data问题，可能还是基于时间窗口部分，本地状态的一个概念？本地缓存的一个概念

规定一个时间段用于重排乱序的事件，同时也具有在一定时间段内重排乱序事件的能力

nosql的一些策略

* hbase
* mongodb

版本和索引的概念

基于raft协议的golang的实现部分？分布式锁的情况，其实没有说到选举部分，这一点有点没答出来，只是有一个分布式锁的状况



关于数据的反馈是如何来做？类似于广告的策略的改变部分？

后面两面部分：

关于数据的质量保证部分，是如何来保证，这个按照面试官的说法，没有答好，这块无非就是事前的测试部分完备性，以及事后的及时恢复性部分，事前测试的完备性，在于一个测试环境以及相关功能的测试的引入，包含流量的复制，相关代码的单测部分，功能测试，压测等等。事后的及时恢复，在于事件的及时通知问题，通知完成之后，自动恢复的部分，对相关数据以及应用的影响部分的预估。

说到自己的缺点的时候，有点太随意了，细节把控部分不仔细，举的例子上，对于上线后的检查部分，有点跟之前质量保证部分，有所冲突。。。有点没答好

关于lazy data的问题，其实做的思路都差不多，将时间戳部分能够继承到后续的数据中，方便进行数据的修正；具有一段时间时间数据的保存功能，方便进行数据的重处理功能

对于系统的复杂性部分，运维上，稳定性上，分布式上，还有灵活性，正确性等等。。



---

今日头条的面试：

* 关于基础知识的不确定，如rune<->int8部分，数据库的索引部分
* 算法的顺序反了。。



关于百度业务的总结，以及分拆部分，



























