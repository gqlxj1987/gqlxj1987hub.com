title: 数学小tips
date: 2018-03-20 09:59:42
categories: math
tags: [math]
---



[链式法则](https://zh.wikipedia.org/wiki/链式法则)

设*f*和*g*为两个关于*x*可导函数，则有下面的公式:
$$
(f\circ g)'(x)=f'(g(x))g'(x)
$$
考虑函数*z* = *f*(*x*, *y*)，其中*x* = *g*(*t*)，*y* = *h*(*t*)，*g*(*t*)和*h*(*t*)是可微函数
$$
\frac{dz}{dx} = \frac{\partial z}{\partial x} \frac{\partial x}{\partial t} + \frac{\partial z}{\partial y} \frac{\partial y}{\partial t}
$$
这个公式是作为梯度下降算法的基本参照