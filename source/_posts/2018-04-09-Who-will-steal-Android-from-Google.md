title: Who will steal Android from Google
date: 2018-04-09 12:04:17
categories: google
tags: [google]
---



[原文链接](https://medium.com/@steve.yegge/who-will-steal-android-from-google-af3622b6252e)



> Why does everyone need mobile devs? Because *the web is slowly dying*. I have friends — well, probably ex-friends now — in just about every org at Google, who used to point me at their gloomy graphs, and it doesn’t matter how you slice it, the web’s in a steady decline as the whole world moves to mobile. 



At Google, most engineers are too snooty to do mobile or web programming. “I don’t do frontend”, they proclaim with maximal snootiness.

There’s a phenomenon there that I like to call the “DAG of Disdain”, wherein DAG means Directed Acyclic Graph, which is a bit like a flowchart

I am talking about full-scale replacements for Google’s *entire*Android development stack. Microsoft has Xamarin, Adobe has Cordova, Facebook has React Native, I mean it’s crazy town.Google has a new Flutter

>  To most folks, they probably appear to be comfortably in the driver’s seat. But consider: If all mobile developers were to start using a particular cross-platform framework X, then literally any other hardware/OS manufacturer or consortium could come along with their own competing hardware/OS platform (like, say, Windows) that supports that framework X directly, and all the apps would run on it (probably faster, to boot), which would cut Google out entirely.

Abandoning native programming in favor of fast-cycle cross-platform frameworks like React Native is a *winning strategy*



conclusion:

尝试下react native？





