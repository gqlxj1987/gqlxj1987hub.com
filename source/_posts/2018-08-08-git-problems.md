title: 10 Git problems
date: 2018-08-08 14:59:49
categories: git
tags: [git]
---



[原文链接](https://citizen428.net/10-common-git-problems-and-how-to-fix-them-e8d809299f08)



`git checkout —` 这样子废弃掉本地的修改



### undo local commit

```shell
git reset HEAD~2        # undo last two commits, keep changes
git reset --hard HEAD~2 # undo last two commits, discard changes  
```



### Remove a file from git without removing it from your file system

```shell
git reset filename          # or git rm --cached filename
echo filename >> .gitingore # add it to .gitignore to avoid re-adding it
```



`git-amend` 操作可以对commit message进行amend操作



### Clean up local commits before pushing

```shell
git rebase --interactive 
# if you didn't specify any tracking information for this branch 
# you will have to add upstream and remote branch information: 
git rebase --interactive origin branch
```





