title: Golang Interface
date: 2018-03-12 11:33:59
categories: golang
tags: [golang]
---



在面向对象的编程中，a protocal or interface is a common means for unrelated objects to communicate with each other

why do we use interfaces?

* writing generic algorithms
* hiding implementation details
* providing interception points



interface need runtime checks

The bigger the interface, the weaker the abstraction



#### abstract data type

