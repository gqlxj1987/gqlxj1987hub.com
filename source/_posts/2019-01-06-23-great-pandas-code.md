title: 23 great pandas code
date: 2019-01-06 14:01:19
categories: pandas

tags: [pandas]
---



[原文链接](https://towardsdatascience.com/23-great-pandas-codes-for-data-scientists-cca5ed9d8a38)



#### Convert data frame to numpy array

```python
df.as_matrix()
```



#### Apply a function to a data frame

This one will multiple all values in the “height” column of the data frame by 2

```python
df["height"].apply(lambda height: 2 * height)
```



