title: Kafka消息格式演进
date: 2017-08-15 07:46:19
categories: kafka
tags: [kafka]
---

[Kafka消息格式演进](https://mp.weixin.qq.com/s?__biz=MzA5MTc0NTMwNQ==&mid=2650714506&idx=1&sn=6499c694e0ab80a8cf0186544507dfd0&chksm=887dacfcbf0a25ea2106ccddbaa8c39bae2f4dd0754a4528bc198d0b8d164115b2ea103db6ff&scene=0&key=c2513fa3664680e1afbe7fd3a425498928f2fd786ff1e2aec7a6432c0b76a93266c56004455bf451e46a889d1b4c2f8fa92d1b13a11bb24441ce32fc6d9f5cd40b54a09cdbb14016f4d3e96cdf57b387&ascene=0&uin=MjkwMTQwODQ2Mg%3D%3D&devicetype=iMac+MacBookPro13%2C3+OSX+OSX+10.12.6+build(16G29)&version=12020810&nettype=WIFI&fontScale=100&pass_ticket=%2FztiEOYTeSZniz8t2qIvpohw2kQwMwIFEXYh6ivBqK5th02GsDrWNoAS8zKbW4eV)

### kafka 0.7.x

Message Format

| magic | Attribute | Crc  | Value |
| ----- | --------- | ---- | ----- |
|       |           |      |       |



Message Set

| offset | size | Message |
| ------ | ---- | ------- |
|        |      |         |



magic: 1个字节,标识kafka版本，1个字节

attribute: 存储消息压缩所使用的编码， 1个字节

crc: 校验消息的内容， 4个字节

value: N-6个字节，N为Message总字节数，


message set中

offset: 8个字节，存储到磁盘之后的物理偏移量

size: 4个字节，消息的大小


发送以message set为单位进行发送，压缩的话，也是以message set的方式进行压缩，也就是value部分，以message set的方式，多条message



### kafka 0.8.x

增加了key相关的信息，以及内容的长度，不再通过特定的N-6这种方式来标明

| crc  | magic | Attribute | key length | key  | value length | Value |
| ---- | ----- | --------- | ---------- | ---- | ------------ | ----- |
|      |       |           |            |      |              |       |



如果进行多条消息的压缩的话，这样，会缺少key部分的存储，而且这时候value为压缩之后的消息内容



### kafka 0.10.x

引入了kakfka stream部分

| crc  | magic | Attribute | Timestamp | key length | key  | value length | Value |
| ---- | ----- | --------- | --------- | ---------- | ---- | ------------ | ----- |
|      |       |           |           |            |      |              |       |



### kafka 0.11.x

较之以前有重大的改变，消息的格式完全变了。。？





