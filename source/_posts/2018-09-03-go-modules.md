title: Go Modules初探
date: 2018-09-03 10:39:15
categories: golang

tags: [golang]
---

[原文链接](https://tw.saowen.com/a/280205cad1502193482905232ba84e501998fcb7216d8e51b037ca892cb22337?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_140)



采用go.mod文件进行管理

`GO111MODULE`环境变量的设置

* off，不使用新特性，继续使用gopath
* on，使用新特性，只根据go.mod来查找
* auto或未设定，



```shell
qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka (module)
$ export GO111MODULE=on  #開啟modules

qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka (module)
$ go mod init  gitlab.luojilab.com/zeroteam/ddkafka # 建立go.mod
go: creating new go.mod: module gitlab.luojilab.com/zeroteam/ddkafka

qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka (module)
$ ls    # 真的建立了，google大法好呀
README.md  go.mod  models.go  mq_interface.go  sarama  segmentio
qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka (module)
$ cat go.mod    # 看看裡面什麼東西
module gitlab.luojilab.com/zeroteam/ddkafka
qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka (module)
$ cd segmentio/
qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka/segmentio (module)
$ go test   # 執行一下看看
go: finding github.com/segmentio/kafka-go latest
go: finding github.com/golang/glog latest
go: downloading github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
go: downloading github.com/segmentio/kafka-go v0.0.0-20180716203113-48c37f796910
qiang@DESKTOP-2A835P9 MINGW64 /d/code/gopath/src/gitlab.luojilab.com/zeroteam/ddkafka/segmentio (module)
$ go list -m
gitlab.luojilab.com/zeroteam/ddkafka
```

go.mod文件可以通過require，replace和exclude語句使用的精確軟體包集。

- require語句指定的依賴項模組
- replace語句可以替換依賴項模組
- exclude語句可以忽略依賴項模組



go mod的一些命令

- go mod init:初始化modules
- go mod download:下載modules到本地cache
- go mod edit:編輯go.mod文件，選項有-json、-require和-exclude，可以使用幫助go help mod edit
- go mod graph:以文字模式列印模組需求圖
- go mod tidy:刪除錯誤或者不使用的modules
- go mod vendor:生成vendor目錄
- go mod verify:驗證依賴是否正確
- go mod why：查詢依賴



可以看出，还是兼容了vendor的用法，同时慢慢支持一些依赖树的构成