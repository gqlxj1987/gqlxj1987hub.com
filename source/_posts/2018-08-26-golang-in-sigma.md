title: Golang in Sigma
date: 2018-08-26 17:05:03
categories: golang
tags: [golang]
---



[原文链接](https://mp.weixin.qq.com/s/sBcaibt2jr3KUTaXMUnwVQ)



![sigma架构](https://mmbiz.qpic.cn/mmbiz_png/yvBJb5IiafvnjrMvAm0ibwmDg3ZCOzjJvw4hCY1thZKjCpibO0cJKKJkeBKlicQFPxNwuNS4AgLa3vicKYXJY5dPjow/640?wx_fmt=png&wxfrom=5&wx_lazy=1)



### Api Server

* 数据一致性

采用redis/etcd，实时+全量的方式

* 状态的一致性

状态的一致性转化为存储的一致性，来降低处理问题的难度

* 高可用-无状态

多master,无状态，以及快速的failover

* 简单
* 降级-抢占



 APIServer 把前面的发布、扩容等都放在 task，丢到 Redis 里，底层的 Worker 消费一些任务，做到无状态

### Scheduler

![并发架构](https://mmbiz.qpic.cn/mmbiz_png/yvBJb5IiafvnjrMvAm0ibwmDg3ZCOzjJvwCXZRLEPjTg1mdyQme7J9dZdXcFiclWZIHqoPRctMI9puuApn68GM9ag/640?wx_fmt=png&wxfrom=5&wx_lazy=1)

整个机器的顶层加一个 glock

加锁部分，尽量在最上层部分？





### map的一些使用

* golang针对map的读写冲突



* map循环中，

```go
for key, value :=range map_obj
```

&value取的value地址不变，而这个地址指向的内容是循环最后一个元素内容



### 超时机制

关于goroutine部分的超时机制



除了滚动式的交付，还有总体任务时间的控制，这就涉及到两级的 Goroute，第一级 Goroute 是总任务，子任务也是 Goroute,这就会涉及到泄露的问题。假设主任务超时了，就不管子任务，子任务一直在跑就会把资源耗完。



k8s中的定时任务部分