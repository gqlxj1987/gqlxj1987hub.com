title: scala学习第二天
date: 2015-07-31 15:07:00
categories: scala
tags: [scala]
---

### 基本类型

Byte Short Int Long Char String Float Double Boolean

管道符号放在字符串前，并且调用stripMargin方法。

符号文本 Symbol 可以是任何字母或数字的标识符 '<标识符>

Int类带有+方法，所以为(1).+(2)

操作符和方法，操作符部分，采用后缀操作符标注方式

对象的相等性 == != 

富包装器 max min abs round

### 函数式对象

```scala
class Rational(n: Int, d: Int) {
	require(d !=0)
	override def toString = n +"/" + d
}
```

自指向: this

从构造器，

字母数字标识符， 操作符标识符

隐式转换
```scala
 implicit def intToRational(x: Int) = new Rational(x)
```

### 内建控制结构

几乎所有的 Scala 的控制结构都会产生某个值

```scala
var filename = 
	if(!args.isEmpty) args(0)
	else "default.txt"
```

for + 过滤

``` scala
val filesHere = (new java.io.File(".")).listFiles
for (file <- filesHere if file.getName.endsWith(".scala"))
println(file)
```

```scala
for (
file <- filesHere
if file.isFile;
if file.getName.endsWith(".scala")
) println(file)
```

for{子语句} yield {循环体}

match表达式替代switch case 

没有break和continue 

变量范围，注意重名


### 函数与闭包

first-class function, 不仅可以定义函数和调用它们，还可以将他们变成值传递。

占位符语法 _ 

偏应用函数

```scala
val b = sum (1, _: Int, 3)
```

函数文本在运行时创建的函数值(对象)被称为闭包，closure。源自于通过“捕获”
自由变量的绑定对函数文本执行的“关闭”行动。

重复参数
```scala
def echo(args: String*) = 
	for(arg<-args) println(arg)
```

尾递归部分



