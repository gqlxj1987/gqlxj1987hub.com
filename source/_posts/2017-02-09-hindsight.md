title: hindsight
date: 2017-02-09 15:12:49
categories: log
tags: [log]
---

[hindsight](https://github.com/mozilla-services/hindsight) 作为hekad部分的替代者，成为日志处理部分的新宠


Agenda:

* 项目起因

* install guide

* use tips

* performance



#### 项目起因

[heka](https://github.com/mozilla-services/heka) 作为使用golang编写，进行相关的日志处理，相比较logstash来说，相关的cpu利用率部分有所降低。同时，相对于logstash来说，支持多种插件(插件采用lua或者go编写)。


##### heka vs logstash

在实际使用过程中，cpu的使用率部分，从400%降至100%左右

logstash部分，采用gork正则的方式来过滤相关的信息

heka部分，亦采用正则，辅之以lua脚本的方式

门槛方面，logstash门槛较低，正则匹配较为简单；heka，需要加入插件部分的支持，同时需要lua脚本的支持



##### heka vs hindsight


hindsight利用[lua sanbox](https://github.com/mozilla-services/lua_sandbox)部分，再辅以[lua_sandbox_extensions](https://github.com/mozilla-services/lua_sandbox_extensions)来支持各种对日志的处理要求


