title: Medium GraphQL server design
date: 2019-01-10 22:37:06
categories: graphql

tags: [graphql]
---



[原文链接](https://medium.engineering/graphql-server-design-medium-34862677b4b8)



**It should be easy to alter the shape of the data**

We currently use [protocol buffers](https://en.wikipedia.org/wiki/Protocol_Buffers) as a schema for data that comes from our backend

**It should be clear what data is for the client**

Within our GraphQL server, data is being passed around and exists in different stages of “readiness” for the client

**It should be easy to add new data sources**



Fetchers, Repositories (Repos), and the GraphQL Schema.



Fetchers should correspond to a REST or preferably a gRPC endpoint. Fetchers require a protobuf. This means that any data that is being fetched by a Fetcher must follow the schema defined by the protobuf



![data flow](https://cdn-images-1.medium.com/max/1600/1*VCs9aXb1RdBFYMhoFJsjjw.png)



The data’s shape becomes more like what the client needs as it passes through each of the distinct layers. It’s clear where the data comes from at each step and we know what each piece of the server is responsible for



关注数据shape

