title: Flux-A New Approach to System Intuition
date: 2018-01-30 14:47:03
categories: flux
tags: [flux]
---



[Flux:A New Approach to System Intuition](https://medium.com/netflix-techblog/flux-a-new-approach-to-system-intuition-cf428b7316ec)

Target:

understand in real time what effect a variable is having on a subset of request traffic during a Chaos Experiment

We require a tool that can give us this holistic understanding of traffic as it flows through our complex, distributed system



requirements:

* Realtime data
* Data on the volume, latency, and health of requests
* Insight into traffic at network edge
* The ability to drill into IPC traffic
* Dependency information about the microservices as requests travel through the system



Pain Suit: As a microservice experiences failure, the corresponding electrodes cause a painful sensation.

主要是针对流量监控部分？



traffic failover?

控制流量流向，

The inter-region traffic from victim to savior increases while the savior region scales up. At that point, we switch DNS to point to the savior region. For about 10 seconds you see traffic to the victim region die down as DNS propagates. At this point, about 56 seconds in, nearly all of the victim region’s traffic is now pointing to the savior region





