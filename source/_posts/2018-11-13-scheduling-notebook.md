title:  notebook的调度
date: 2018-11-13 22:26:33
categories: scheduling

tags: [scheduling]
---



[原文链接](https://medium.com/netflix-techblog/scheduling-notebooks-348e6c14cfd6)



We’re currently in the process of migrating all **10,000** of the scheduled jobs running on the Netflix Data Platform to use notebook-based execution



**Notebooks are, in essence, managed JSON documents with a simple interface to execute code within.**



Notebooks pose a lot of challenges: they’re frequently changed, their cell outputs need not match the code, they’re difficult to test, and there’s no easy way to dynamically configure their execution. Furthermore, you need a notebook server to run them, which creates architectural dependencies to facilitate execution.



