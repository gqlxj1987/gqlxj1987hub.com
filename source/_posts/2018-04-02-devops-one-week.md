title: devops一周总结
date: 2018-04-02 10:22:09
categories: devops
tags: [devops]
---



### Runbook的实践

这篇文章讲述了这个小团队如何让两三个人做 OnCall，但其实讲述的是 Runbook 的一些实践。

- Runbook 是用来给 24x7 团队在非工作时间不用召唤开发的情况下可以自己根据文档运行一些基本故障的对应解决方案。
- 原则：如果命令未来可能会被再次执行，那就花点时间写点文档，写清楚执行的步骤。
- Runbook 尽可能按照一个模板来写，有一些基本的信息，比如有哪些环境，怎么找日志，怎么做 troubleshooting。
- 代码库里面嵌入一些 scripts, 方便执行。
- 如果 Runbook 来不及更新，好歹留一条评论在那里。
- Runbook 有个 owner，出问题好抓人。





