title: Kubernetes Monitor Pipeline
date: 2019-01-03 22:47:05
categories: kubernetes

tags: [kubernetes]
---



[原文链接](https://medium.com/magalix/kubernetes-monitoring-101-core-pipeline-services-pipeline-a34cd4cc9627)



* **The core metrics pipeline**, which is an integral part of Kubernetes and always installed with all distributions
* **The services monitoring (non-core) pipeline**, which is a separate pipeline, and Kubernetes has no or limited dependency on



core metrics workflow

1. CAdvisor collects metrics about containers and nodes that on which it is installed. Note: CAdvisor is installed by default on all cluster nodes
2. Kubelet exposes these metrics (default is one-minute resolution) through Kubelet APIs.
3. Metrics Server discovers all available nodes and calls Kubelet API to get containers and nodes resources usage.
4. Metrics Server exposes these metrics through Kubernetes aggregation API.





servies monitor workflow

1. Monitoring agent collects node metrics. cAdvisor collects containers and pods metrics.
2. Monitoring Aggregation service collects data from its own agent and cAdvisor.
3. Data is stored in the monitoring system’s storage.
4. Monitoring aggregation service exposes metrics through APIs and dashboards.



