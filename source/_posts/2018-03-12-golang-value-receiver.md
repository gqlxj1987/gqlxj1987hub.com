title: Constructors In Go
date: 2018-03-12 11:18:29
categories: golang
tags: [golang]
---



[原文链接](https://scene-si.org/2018/03/08/an-argument-for-value-receiver-constructors/?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_118)



There are objects in Go that have to be initialized, for example channels and slices immediately come to mind. You’ll have to call `make` to set that up.

>“The make built-in function allocates and initializes an object of type slice, map, or chan (only). Like new, the first argument is a type, not a value. Unlike new, make’s return type is the same as the type of its argument, not a pointer to it.”



关于constructor部分？



#### value receivers

```go
type Person struct {
    name string
}

func (Person) New(name string) *Person {
    return &Person{name}
}
```



And to use such a constructor, you would invoke `Person{}.New("Tit Petric")` and end up with the initialized object. We can in fact reason better about the code we are writing, because we can asume that we’ll end up with a `Person` object (or a pointer to it), because that’s what we start with



there’s no performance or memory impact from using value receivers over usual function constructors

```go
New         2000000     754 ns/op     0 B/op      0 allocs/op
Person.New  2000000     786 ns/op     0 B/op      0 allocs/op
```

原因在于：The constructor code is completely the same, and in both cases, the functions get fully inlined, so whichever format you prefer to write results in completely the same assembly code output



#### conclusion

While value receiver constructors do feel *slightly* awkward, it’s as close to analogous copy of constructors from other languages. While in PHP the constructor would be invoked when you call `new Person(...)` and would only return objects of that type, Go actually allows you to go beyond that with `Person{}.New()`.

