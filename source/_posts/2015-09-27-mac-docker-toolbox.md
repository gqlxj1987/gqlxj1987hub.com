title: Mac安装docker折腾记
date: 2015-09-27 18:00:41
categories: docker
tags: [docker]
---

### Enviroment
* OS X 10.9.5
* VritualBox 4.3.31
* docker toolbox  1.8.2a

### Pre-Condition

```shell
$ VBoxManage list vms
$ 
```
```shell
$ VBoxManage list hostonlyifs
$
```
```shell
$ docker-machine ls
NAME   ACTIVE   DRIVER   STATE   URL   SWARM
```

### tips

因为这个版本在mac上的bug，主要是由于multiple ssh的问题，导致创建虚拟机无法成功


> Just FYI everyone, if the reason you are encountering these issues is because of SSH multiplexing configuration settings, it should be fixed in the next release / on master.

故需要将自己的.ssh/config文件设置为类似如下：

```shell
% cat ~/.ssh/config
Host *
  TCPKeepAlive yes
  ServerAliveInterval 10
  ServerAliveCountMax 10
  ForwardAgent yes
#  ControlMaster auto
#  ControlPath ~/.ssh/sockets/%r@%h-%p
#  ControlPersist 300
```
即去掉相关controlPath相关的内容。

这块后续可以试验一下，当run起来vm后，再改变相关的ssh文件是否ok？

### commnad

```shell
$ docker-machine --debug create -d virtualbox default --native-ssh
```
虽然过程中有报错，但好歹也算ok了

```shell
> docker-machine ls
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM
default            virtualbox   Running   tcp://192.168.99.100:2376 
```

随后就是普通的设置相关的环境

```shell
> docker-machine --native-ssh env default
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.100:2376"
export DOCKER_CERT_PATH="/Users/gqlxj1987/.docker/machine/machines/default"
export DOCKER_MACHINE_NAME="default"
# Run this command to configure your shell: 
# eval "$(docker-machine env default)"

```

```shell
> eval $(docker-machine --native-ssh env default)
```

enjoy docker!


