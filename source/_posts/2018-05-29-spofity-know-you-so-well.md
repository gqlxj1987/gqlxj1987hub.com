title: spotify know you so well
date: 2018-05-29 11:06:37
categories: spotify
tags: [spotify]
---



[原文链接](https://medium.com/s/story/spotifys-discover-weekly-how-machine-learning-finds-your-new-music-19a41ab76efe)



1. **Collaborative Filtering** models (i.e. the ones that Last.fm originally used), which analyze both *your* behavior and *others’* behaviors.
2. **Natural Language Processing (NLP)** models, which analyze *text.*
3. **Audio** models, which analyze the *raw audio tracks* *themselves*.



![discover weekly](https://cdn-images-1.medium.com/max/1600/1*cp07MRMUjndZsvV7QElSXg.png)



Unlike Netflix, Spotify doesn’t have a star-based system with which users rate their music. Instead, Spotify’s data is **implicit feedback** — specifically, the **stream counts** of the tracks and additional streaming data, such as whether a user saved the track to their own playlist, or visited the artist’s page after listening to a song.

根据喜欢的音乐，来归类于相似用户。

In actuality, this matrix you see here is *gigantic*. **Each row represents one of Spotify’s 140 million users —** if you use Spotify, you yourself are a row in this matrix — and **each column represents one of the 30 million songs** in Spotify’s database.



The exact mechanisms behind NLP are beyond the scope of this article, but here’s what happens on a very high level: Spotify crawls the web constantly looking for blog posts and other written text about music to figure out what people are saying about specific artists and songs — which adjectives and what particular language is frequently used in reference to those artists and songs, and which other artists and songs are also being discussed alongside them.



with cnn处理audio部分

