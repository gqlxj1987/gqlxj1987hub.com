title: Go std doc
date: 2018-05-24 15:36:32
categories: golang
tags: [golang]
---



[原repo](https://github.com/preytaren/go-doc-zh)



## container/heap

fix用于单次对堆进行调整

```go
func Fix(h Interface, i int)
```

关于大顶堆以及小顶堆



## container/list

基本的双向链表

```go
package main


import (
"fmt"
"container/list"
)


func main() {
	link := list.New()

	for i := 0; i <= 10; i++ {
		link.PushBack(i)
	}//

	for p := link.Front(); p != link.Back(); p = p.Next() {
		fmt.Println("Number", p.Value)
	}

}
```



## container/ring

循环表结构

Do, Ring包提供一个额外的方法Do，会依次将每个节点的Value当做参数调用这个函数

```go
package main

import (
	"container/ring"
	"fmt"
)

func main() {
	r := ring.New(10)

	for i := 0; i < 10; i++ {
		r.Value = i
		r = r.Next()
	}

	sum := SumInt{}
	r.Do(func(i interface{}) {
		fmt.Println(i)
	})
}
```



求和的写法：

```go
package main

import (
	"container/ring"
	"fmt"
)

type SumInt struct {
	Value int
}

func (s *SumInt) add(i interface{}) {
	s.Value += i.(int)
}

func main() {
	r := ring.New(10)

	for i := 0; i < 10; i++ {
		r.Value = i
		r = r.Next()
	}

	sum := SumInt{}
	r.Do(sum.add)
	fmt.Println(sum.Value)
}
```



## sort

```go
type Interface interface {
        Len() int            // 返回当前元素个数
        Less(i, j int) bool. // 判断第i个元素是小于第j个元素
        Swap(i, j int)       // 交换两个元素
}
```



只要实现这三个函数，即可使用sort的相关函数

Slice

Slice函数用于对一个Slice进行排序，这是实际使用中更为常用的一个函数，函数接收两个参数。第一个是需要排序的Slice；第二个是Slice元素比较函数，它类似于前面sort.Interface里的Less方法。函数声明如下：

```go
func Slice(slice interface{}, less func(i, j int) bool)
```



## sync

信号量，sync.Cond是信号量的实现

Signal和Broadcast用于唤醒一个信号量，区别在于Signal只会随机的唤醒一个线程，而Broadcast会唤醒所有在等待的线程。

```go
func (c *Cond) Signal()

func (c *Cond) Broadcast() 
```

wait

阻塞等待，当Signal或Broadcast被调用时唤醒，声明如下：

```go
func (c *Cond) Wait()
```



sync.Once保证某个函数有且仅有一次执行，只有一个方法Do，接受一个无参函数作为参数，当你调用Do时会执行这个函数，其方法声明如下：

```go
func (o *Once) Do(f func())
```



## context

context也是并发环境的一个常用标准库，它用于在并发环境下在协程之间安全的传递某些上下文信息。

一个经典的应用场景是服务器模型，当服务器处理接收到的请求时，通常需要并发的运行多个子任务，例如访问服务器，请求授权等。而这些任务都会以子协程的方式运行，也就是说一个请求绑定了多个协程，这些协程需要共享或传递某些请求相关的数据；此外当请求被撤销时，也需要有一种机制保证每个子协程能够安全的退出。而context包就给提供了上面说到的这些功能。



Context是一个上下文对象，其声明如下：

```go
type Context interface {
    Deadline() (deadline time.Time, ok bool)
    // 获取deadline
    Done() <-chan struct{}
    // 
    Err() error
    Value(key interface{}) interface{}
}
```



