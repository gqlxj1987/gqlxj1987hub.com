title: Function Program
date: 2017-04-01 15:49:18
categories: function program
tags: [function program]
---

[原文](https://davidwalsh.name/functional-programming?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_79)


offers a helpful twist on typical functional programming operations (like `map(..)`, `compose(..)`, etc)

主要是从JS的角度来说明functional programming，例如，ES6

大量的[FP libraries in JS](https://github.com/stoeffel/awesome-fp-js#libraries)

```javascript

function uppercase(v) { return v.toUpperCase(); }

var words = ["Now","Is","The","Time"];
var moreWords = ["The","Quick","Brown","Fox"];

var f = R.map( uppercase );
f( words );                        // ["NOW","IS","THE","TIME"]
f( moreWords );                    // ["THE","QUICK","BROWN","FOX"]

```

使用Ramda, `R.map(..)`是柯西化的

我们注意到这部分的功能是，先使用mapper的功能，也就是uppercase的分发，然后才是arrays的功能(遍历？)

因为是柯西化的，所以我们可以进行反转

```javascript

var p = R.flip( R.map )( words );

p( lowercase );                 // ["now","is","the","time"]
p( uppercase );                 // ["NOW","IS","THE","TIME"]

```

这样，先进行遍历array的操作，然后才是相关功能的mapper? 这里面使用了`R.flip(..)`这样的功能进行参数的交换

**由此引出的问题，在`functional programming`中，需要记住参数的顺序**


```javascript

function concatStr(s1,s2) { return s1 + s2; }

var words = ["Now","Is","The","Time"];

_.reduce( concatStr, _, words );
// NowIsTheTime

_.reduce( concatStr, "Str: ", words );
// Str: NowIsTheTime

```

这里面`_.reduce(..)`使用这样的参数顺序`reducerFunction, initialValue, arr`。但我们一般情况下很少填写`initialValue `

**这样的一个解决办法就是`Named Arguments`。这样的一种方案不是跟python很类似？**







