title: k8s 概念Intro
date: 2019-01-08 19:31:56
categories: kubernetes

tags: [kubernetes]
---



[原文链接](https://enqueuezero.com/kubernetes-in-a-nutshell.html)



master->node->pod->replicaSet->deployment



As a Kubernetes user, you might most want to ask Kubernetes to **run a container** via HTTP APIs or CLI.

In other words, you set a desired state to the **Master** server.

The **Master** server compares the desired state to the current state in the cluster and decides which node to run.

**Node** servers receive instructions from the **Master** server and then starts a container in the runtime.



Except the container runtime, the **Node** server runs **kubelet** and **kube-proxy**.

**Kubelet** communicates to the **master** to receive desired states.

**Kubelet** controls the **container runtime** to launch or destroy containers to match the desired states.

**Kube-proxy** forwards requests to the containers, either in localhost or other hosts.



A **Pod** is one or more containers in a group that should always run on the same **node**.

All containers in a **pod** are launched and destroyed together, or share a life cycle.

All containers in a **pod** share their environments, volumes, and IP space.

Usually, there is one main container and some optional **sidecar containers** in a **pod**.



A **ReplicaSet** is one or more pod replicas that are running at any given time.



A **Deployment** is a desired state for **ReplicaSets**.

The **deployment controller** creates new replicasets for the rolling updates and then replaces the current replicaset.

---

order kill

To support the ordering and uniqueness of Pods, Kubernetes offers **StatefulSet**.

---

For applications like logging agents, we want to deploy them per host.

To support such feature, Kubernetes offers **DaemonSet**.

It ensures that all (or some) Nodes run a copy of a Pod.

As nodes are added to the cluster, Pods are added to them.

As nodes are removed from the cluster, those Pods are garbage collected.

Deleting a DaemonSet will clean up the Pods it created.

---

In case you have one-off jobs, Kubernetes offers **Jobs**.

A **job** creates one or more pods and ensures that a specified number of them successfully terminate.

When a specified number of successful completion is reached, the job itself is complete.

Deleting a Job will clean up the pods it created.

---

pods communicate to other pods use **Services**

A **Service** is a component that acts as a basic internal load balancer for pods.

The **Service** exposes a group of pods as a single entity. When one pod communicates to a service, the service proxies the request to one of the backend pods.



service -> dns -> pod



What if one service wants to expose itself to the external world? Kubernetes offers **Ingress**.



