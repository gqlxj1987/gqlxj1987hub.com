title: 10.11一周总结
date: 2015-10-11 10:42:08
categories: weekly
tags: [weekly]
---

本周总结：

* 打包平台一期基本完成
* 前端页面改版基本完成
* 账户中心demo支持第三方通过cookies登录
* falcon监控部分完成

未完成：

* uuap的原理的场景图
* 前端部分优化


本周所得：

* spring boot中mvc的部分以及通过httpBasic中的header部分来验证
* docker部分mysql,redis以及es使用正常

下周计划：

* 想通uuap的流程，以及利用session来登录
* 前端访问优化的一些策略进行实践，例如图片压缩，以及合并等


帐号中心，总感觉差一张纸，有些东西没有想透，导致现在有点进度缓慢，这块后续要加强

docker部分，自己的机器搭一个docker register来存放镜像？

强制使用下tmux来使用命令行的相关内容

