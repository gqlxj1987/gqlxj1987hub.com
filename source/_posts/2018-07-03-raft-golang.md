title: raft协议简介
date: 2018-07-03 16:32:29
categories: raft
tags: [raft]
---



[原文链接](http://thesecretlivesofdata.com/raft/)



node存在三种状态， follower, candidate or leader，如果没有收到相关的entries的话，会从follower变迁到candidate，从而进行leader的一个选举策略



两个时间超时的机制

* 选举超时，如果超时，就会变成candidate，开始进行vote，并发向其他的node
* hearbeat 超时，在vote发送之后，有没有在超时之前收到相关的vote的答复



每次leader发送log时，都会去重置其他follower节点的选举超时时间



这时候，会遇到多个nodes同时发送选举的信息，继续等待，直到有一个拿到master的选举信息，



然后是关于网络隔断的问题，在移除之后，有所谓的值最大的成为最后的master







