title: Committing every day
date: 2017-03-05 10:21:40
categories: weekly
tags: [weekly]
---

I really believe committing every day on an open source project is the best practice.



本周完成：

* 取数据部分的及时性和便捷性


* 日志的归一化部分，由于涉及到callback部分，暂不可替server端日志


* 加入keyword策略部分的统计，在线以及离线


* 收入抓取和报警部分调整


未完成：

* 渠道监控后续的工作？


* 现有的服务点的改造




本周所得：

* golang-101-hacks

* python bs4部分，有关session以及https部分的抓取


下周计划：

* 规范一些离线报告的范畴


* 梳理现有的服务的改造点


* 关于ecpm的分时报告部分



技术文章，每周两篇，没啥问题


关于project52部分，组织一下项目结构部分，慢慢写一些工作log


尝试写下每周阿南的读后感，尝试向开源部分靠靠。。


尝试控制自己的欲望，推迟一些不必要的想法和成就感。。



