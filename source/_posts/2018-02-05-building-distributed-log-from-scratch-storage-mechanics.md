title: Building distributed log from scratch With Storage Mechanics
date: 2018-02-05 16:55:09
categories: storage
tags: [distributed, storage]
---

[原文链接](https://bravenewgeek.com/building-a-distributed-log-from-scratch-part-1-storage-mechanics/)



日志是完全顺序，只接受append操作的数据结构。



I think there are at least three key priorities for the effectiveness of one of these types of systems: performance, high availability, and scalability. If it’s not fast enough, the data becomes decreasingly useful. If it’s not highly available, it means we can’t reliably get our data in or out. And if it’s not scalable, it won’t be able to meet the needs of many enterprises.



