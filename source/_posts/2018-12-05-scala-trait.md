title: scala trait
date: 2018-12-05 22:52:51
categories: scala

tags: [scala]
---



[原文链接](http://weinan.io/2018/11/05/scala.html)



trait并不是interface，而是一个功能模块，允许用户把这个模块插入到class中



trait本身也可以继承，方法也可以直接在trait里实现



```shell
scala> val service2 = new ServiceImportante("dos") with StdoutLogging {
     |   override def work(i: Int): Int = {
     |     info(s"Starting work: i = $i")
     |     val result = super.work(i)
     |     info(s"Ending work: i = $i, result = $result")
     |     result
     |   }
     | }
service2: ServiceImportante with StdoutLogging = $anon$1@674fee6d
```



当然也可以重复使用`ServiceImportante with StdoutLogging`，定义一个新的class



