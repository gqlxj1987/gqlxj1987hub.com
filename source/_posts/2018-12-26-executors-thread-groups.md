title: Executors同Thread Groups
date: 2018-12-26 22:16:16
categories: java

tags: [java]
---



[原文链接](http://weinan.io/2018/01/24/executors.html)



```java
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorsDemo {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.submit(() -> {
			try {
				Thread.currentThread().wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		// main thread会在这里block住。
	}
}
```



猜测在同一个thread group的thread会互相block住

```java
public class PlayWithDefaultThreadFactory {
	public static void main(String[] args) {
		DefaultThreadFactory factory = new DefaultThreadFactory(); // 这个DefaultThreadFactory会把新的thread创建在main thread所属的group里。

		// 同一个thread group里面的thread执行没结束的时候，main thread不会退出，会被block住。
		Thread t = factory.newThread(() -> {
			try {
				sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		// 这个thread跑起来以后，main thread会继续执行，直到block在结束的地方，等待t里面的sleep的3秒钟完成，除非像下面这样给interrupt()。
		t.start();

        // 让worker thread直接退出，这样main thread就不会block在结束的位置了
        t.interrupt();
	}
}
```

