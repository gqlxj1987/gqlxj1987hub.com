title: Chaos Engineering
date: 2018-03-27 16:13:10
categories: chaos
tags: [chaos]
---



[原文链接](http://principlesofchaos.org/)

最重要的问题：

> How much confidence we can have in the complex systems that we put into production?



Even when all of the individual services in a distributed system are functioning properly, the interactions between those services can cause unpredictable outcomes.  Unpredictable outcomes, compounded by rare but disruptive real-world events that affect production environments, make these distributed systems inherently chaotic

不要有放松的心理



- Chaos Engineering 的定义：是一种对分布式系统做线上的测试的实践，手段是在一个隔离的生产环境中人为制造一些极端环境。
- 意义：即便每个组件都是好的，他们通讯出问题了，系统还是一样照挂；或者服务降级是配置有问题，超时不合适导致过多重试，服务要承受太多流量，单点故障导致雪崩。分布式系统会出现的情况实在太多了。预期等着出现，不如主动出击。主动制造问题，如果制造出的问题怎么搞系统都不会崩，那系统看起来还蛮可靠的。



- Chaos 四个步骤：
  - 定义系统正常时的一些指标，收集监控数据。
  - 假设在 control group 和 experiment group 两个都处于正常状态。
  - 制造事故，模拟出 server crash，硬盘故障，网络连接故障等等。
  - 尝试找出 control group 和 experiment group 的状态的不同。如果有不同，那就找到了可以改进的地方。





- 一些实践：
  - 与其搜集系统的内部属性指标，不如搜集系统的输出。吞吐，错误率，latency 百分位这些都是蛮好的指标。这些通用指标能证明系统正在工作。
  - Chaos variables 是一些跟现实中会可能是系统出现故障的那些情况，不一定会是硬件出问题，甚至网络流量出现 spike 都可能可以算。
  - 生产环境的数据很宝贵，可以做采样，然后对实验环境进行测试。
  - 对这些实验写代码，做自动化。
  - 最小化对客户的影响，限制在阈值内

