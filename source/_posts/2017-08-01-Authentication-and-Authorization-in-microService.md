title: Authentication and Authorization in MicroService
date: 2017-08-01 00:29:18
categories: auth
tags: [auth]
---

[How We Solved Authentication and Authorization in Our Microservice Architecture](https://medium.com/technology-learning/how-we-solved-authentication-and-authorization-in-our-microservice-architecture-994539d1b6e6)

### Architecutre

Building Blocks of our authentication/authorization layer

Token Based Authentication

>When a user logs in successfully, a jwt is returned. On subsequent requests, the user must attach this token to each request. This is needed so that the api gateway can establish the identity of the user and extract the user’s claims from the token.


Single SignON(SSO) 

> The way SSO is implemented in our system is simple. When a user logs in for the first time from any frontend app, a cookie called jwt-token gets created on the api-gateway. The cookie’s domain is .andela.com and hence accessible to all andela.com subdomain

针对mobile apps部分

>the api-gateway supports passing the jwt via Authorization header as Bearer Tokens



### Authentication

The api gateway

### Authorization

permissions in authorization service has many to many relationship with roles in user service

权限模型部分