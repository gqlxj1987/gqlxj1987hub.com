title: Vitess Intro
date: 2018-09-30 18:48:58
categories: vitess

tags: [vitess]
---

[原文链接](http://jixiuf.github.io/blog/go_vitess.html/)



![Architecture](http://jixiuf.github.io/assets/blog/go_vitess.html/VitessOverview.png)



* vtgate 可以认为是网关,client 只需要与vtgate 连接即可，vtgate 会把相应的sql 路由到相应的vttablet 进行查询 执行等，vtgate 可以启动多个，来做负载均衡，相当于proxy部分
* vttablet 可以认为是在mysql 前面挡了一层，一个vttablet 对应一个mysql 实例，主要提供连接池、查询重写、查询去重、以及相应的管理操作
* vtctl 命令行， 提供管理vitess 的命令等，包括查询master-slave关系，sharding 信息。例如，建表，执行sharding resharding ，执行failover(切换主从关系等)等操作。
* vtworker 执行一些需要长时间运行的进程。它支持一个插件式的架构并提供了第三方库，这样你可以轻易选择要使用的 tablet。该插件可以用于以下类型的工作：
  * resharding differ：在水平分片的分割以及合并时核查数据完整性的工作
  * vertical split differ：在垂直分割以及合并时核查数据完整性的工作
  * vtworker 还允许您轻松地添加其他验证程序。你可以进行 in-tablet 完整性检查以验证外键之类的关联关系或者跨片完整性检查，例如，一个密钥空间里的索引表所指向的数据在另一个密钥空间里。



Vitess 使用基于范围的分片。Vitess 使用一个数据存储一致性的拓扑支持，比如 etcd 或者 ZooKeeper。这也就意味着集群视图始终是最新的而且对于不同的客户端也能始终保证其一致性。



Keyspace对应着mysql逻辑上的一个database，那么他的关键的就是做sharding，还有作为读写分离部分



一个tablet 包含

1. 一个mysql instance
2. 一个 vttablet instance
3. 一个可选的row cache instance (memcache)
4. 其他一些特定的database 相关进程

类型

1. master 主库
2. replica 低延迟的从库
3. rdonly 延迟相对较高的只读库，主要执行一些后台耗时操作
4. spare 暂时不工作的slave





