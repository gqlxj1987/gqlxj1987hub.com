title: 葡萄成熟时
date: 2016-12-06 00:36:48
categories: weekly
tags: [weekly]
---

你要静候，再静候，就算失收，始终要守。。


本周工作：

* 进行zhulong平台部分数据的提供


* cpi手动录入完成


* 收入抓取部分的support



未完成：

* 收入部分的定制


* 关于离线数据如何恢复的问题


* 关于各个topic部分的数据报警，还是各个log解析部分的报警？



下周计划：


* 图形展示

* zhulong部分验收

* 内部收入部分整理


本周所得

* haskell语言的学习

* scala lzo部分

* python抓取部分cookies部分？https？



时间表上，注意慢慢调整回来，列一下时间表，本月看几本书


锻炼方面，朝着半马慢慢进发


golang部分，可以准备开始启动project 52。







