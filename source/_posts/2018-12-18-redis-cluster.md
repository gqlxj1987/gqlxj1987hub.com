title: redis集群下key的一些策略
date: 2018-12-18 19:55:23
categories: redis

tags: [redis]
---



| 压测client             | 压测server            | 线程/进程 | 最大qps | command | args | 是否pipeline | 集群是否指定slot |
| ---------------------- | --------------------- | --------- | ------- | ------- | ---- | ------------ | ---------------- |
| tencent-recom-center01 | cluster               | 20        | 522     | mget    | 100  | n            | n                |
| tencent-recom-center01 | tencent-recom-redis02 | 20        | 7200    | mget    | 700  | n            | n                |
| tencent-recom-center01 | tencent-recom-redis02 | 20        | 1418    | lrange  | 100  | n            | n                |
| tencent-recom-center01 | tencent-recom-redis02 | 20        | 11435   | lrange  | 100  | n            | n                |
| tencent-recom-center01 | tencent-recom-redis02 | 20        | 11295   | lrange  | 100  | y            | n                |
| tencent-recom-center01 | cluster               | 40        | 12300   | lrange  | 100  | n            | n                |
| tencent-recom-center01 | cluster               | 40        | 15354   | lrange  | 100  | y            | n                |
| tencent-recom-center01 | cluster               | 40        | 12378   | lrange  | 100  | n            | y                |
| tencent-recom-center01 | cluster               | 40        | 13618   | lrange  | 100  | y            | y                |



测试主要集中在mget以及lrange部分



mget部分

对于mget命令，在集群执行时，相比对单点redis执行，会增加了拆分成get的多次网络连接成本，因此效率会大大降低。解决方案是将需要再同一条mget命令出现的key写在同一个slot上，这样的话jedis客户端会将mget保留并传给对应的redis node，而不是拆分成多个get



关于slot的使用

因此对于需要mget的key需要手动分配到同一个slot上，官方给出的具体方式是使用{}将需要hash计算的部分扩起来，这样只要{} 内包含的字段一样的key，都会被分配到同一个slot上，例如下面这三条会被分配到同一个slot上

```shell
{recom:item_123:i2i}:xxxxxxxxxxxxx
{recom:item_123:i2i}:qqqqqqqqqqqqq
{recom:item_123:i2i}:fffffffffffff
```

扩起来的话，具体的写法是什么样的？



```shell
CLUSTER KEYSLOT "{lewis1}5400"       #查看key存在于哪个slot上
 
(integer) 938
 
CLUSTER KEYSLOT "{lewis1}421"        #括号部分内容一样的key会被hash到同一个slot上
 
(integer) 938
```



这样写的情况下，手动指定的情况下，与codis的区别是？



