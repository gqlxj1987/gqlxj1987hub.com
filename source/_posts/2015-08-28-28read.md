title: 今日阅读
date: 2015-08-28 08:11:38
categories: 架构设计
tags: [架构]
---

### 架构设计

设计一个现代化的开发框架要考虑到

1. 降低工程师的开发负担
2. 尽量限制工程师的不良习惯
3. 帮助运营 ... 

而「帮助运营」还可以再细分成三部分 1. 帮助在云平台上运维 2. 帮助大数据的收集 3. 帮助开放平台的建立

在我的分层架构中，我现在建议的开发次序是：Feature -> Storyboard (UI-Flow) -> Application Glue -> API -> Data Model -> Domain Model -> SPI -> SPI Impl. -> Service -> API Impl

架构设计的知识分成四个层次：架构原则（例如系统间不能共用数据库）、架构设计模式（例如 MVC）、架构模型（例如我的 3D 架构模型）、架构风格（例如 EDA）