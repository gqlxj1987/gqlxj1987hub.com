title: Argo Architecture
date: 2018-12-20 19:26:27
categories: argo

tags: [argo]
---



[repo地址](https://github.com/argoproj/argo)



argo是一个Open source Kubernetes native workflows, events, CI and CD

这里主要是介绍它的events机制，也就是所谓的事件驱动？



- `gateways` which are implemented as a Kubernetes-native Custom Resource Definition that either produce the events internally or process the events that originate from outside the gateways
- `sensors` which are implemented as a Kubernetes-native Custom Resource Definition that define a set of dependencies and trigger actions.
  - Define multiple dependencies from a variety of gateway sources
  - Build custom gateways to support business-level constraint logic
  - Trigger messages and Kubernetes object creation after successful dependency resolution
  - Trigger escalation after errors, or dependency constraint failures
  - Build and manage a distributed, cross-team, event-driven architecture
  - Easily leverage Kubernetes-native APIs to monitor dependencies

gateways作为producer，sensors的作用？



