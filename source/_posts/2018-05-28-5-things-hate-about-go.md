title: 5 things hate about go
date: 2018-05-28 17:12:57
categories: golang
tags: [golang]
---



[原文链接](https://jaxenter.com/5-things-you-hate-about-go-143422.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_129)



My personal favorite is that Go does not offer a *reentrant lock*, i.e. a lock that can be recursively acquired from the same thread or Goroutine (the Go variant of Coroutines or *Green Threads*). However, without hacks there is no way to build such an implementation by yourself, since threads are not available in Go and Goroutines do not offer an identifier, which would allow you to recognize the same Goroutine you’ve seen before.

```go
type task struct {
}
  
func main() {
  task := &task{}
  task = &task{}
}
```

At this point, Go is no longer able to see the distinction between the type and the variable

### **Type or no type, that is the question!**

```go
type handle = int // after go 1.9
type handle int // before go 1.9
  
func main() {
  var var1 int = 1
  var var2 handle = 2
  types(var1)
  types(var2)
}
  
func types(val interface{}) {
  switch v := val.(type) {
  case int:
    fmt.Println(fmt.Sprintf("I am an int: %d", v))
  }
  switch v := val.(type) {
  case handle:
    fmt.Println(fmt.Sprintf("I am an handle: %d", v))
  }
```

### Gophers are lazy by nature!

```go
func main() {
  functions := make([]func(), 3)
    for i := 0; i < 3; i++ {
      functions[i] = func() {
      fmt.Println(fmt.Sprintf("iterator value: %d", i))
      }
    }
  
  functions[0]()
  functions[1]()
  functions[2]()
}
```

###  Of nil and nothing

```go
func test(v bool) error {
  var e *MyError = nil
  if v {
    return nil
  }
  return e
}
```

When `e` is returned, the `*MyError` pointer becomes an instance of the interface type `error` and it’s not nil! 



