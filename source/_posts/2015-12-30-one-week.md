title: 12.30一周总结
date: 2015-12-30 00:06:53
categories: weekly
tags: [weekly]
---

本周完成：

* textcutie部分，完成python的s3上传
* 测试环境方面，尝试建一套测试环境
* api规范部分输出
* beego框架熟悉，以及go语言学习



未完成：

* 统计的自动化部分
* es的自动重启部分？



本周所得：

* python的相关经验，以及利用screen的命令来进行相关工作
* go语言的学习

下周计划：

* api迁移的计划表部分
* sticker的api完成



关于分享的ppt部分，是不是尝试先做一些。。

go部分的学习，要加快一些，多用一些语言特性，多用一些开源库

