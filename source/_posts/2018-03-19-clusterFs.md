title: ClusterFS纠错码设计
date: 2018-03-19 18:39:20
categories: clusterfs
tags: [clusterfs]
---



[原文链接](http://www.taocloudx.com/index.php?a=shows&catid=4&id=104)



副本以及纠错码的方式，副本读写效率高，但浪费空间，

EC纠错码，Erasure Code，使用RS(Reed-Solomon)码算法对原始数据进行编码，在原始数据中加入redundancy，之后将编码文件分片存储到volume中的相应bricks中



RS(Reed-Solomon)码有两个参数n和m，记为RS(n,m)，n代表原始数据块个数，m代表校验块个数

Glusterfs文件系统模块化的通过各种Translator(一种强大的文件系统扩展机制)链接起来实现各种功能，各Translator在需要运行时通过动态库的方式加载。其中Cluster translator 是Glusterfs实现存储功能的核心，实现了DHT、AFR、EC等重要功能。

**Usable size = Brick size \* (#Bricks - Redundancy)**

**0< redundancy<#Bricks /2**

计算方式：

其中，#Bricks表示每个Dispersed Set中的brick总数，例如：

1）3 bricks，创建Dispersed Type为1*(2+1)，存储磁盘空间利用率66.7%；

2）10bricks，创建Dispersed Type为2*(4+1)的卷，存储磁盘空间利用率为80%。



