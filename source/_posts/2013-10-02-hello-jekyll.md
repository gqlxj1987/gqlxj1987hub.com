---
layout: post
title: "Hello Jekyll"
description: ""
category: ""
tags: [Jekyll]
---

这是在**[jekyll][1]**上的第一篇文章，记录一下自己工作之后写博客的开始，希望自己能够坚持下去。加油！
<!-- More -->
首先是jekyll的搭建。

* railsinstaller的安装。
	* 关于在mac上，使用rvm的安装来安装ruby。PS:在这一点上，鉴于mac lion上的ruby为1.8.7,故需要升级。但由于jekyll在运行时对ruby的版本的要求为1.9.2，对于版本低或者高都不认。
	* 对于ruby 1.9.2的安装，采用brew去安装，但sf被墙，无法配置pkg-config。采用`sudo rvm install ruby 1.9.2`的命令去安装ruby 1.9.2可以成功，但`rvm install ruby 1.9.2` 却各种报错，最后采取终极办法，将系统的`ruby`的地址改为安装好的1.9.2的`ruby`的地址，颇有linux内核编译的感觉。。
* `gem install jekyll`,然后是在jekyll-bootstrap上去选取一个合适的主题
* `jekyll --server`本地运行ok，即it works

然后是关于markdown的学习。

* 区块元素
	* &copy; 特殊字符的表示
	* 段落和换行
		* 标题 行首插入#		
###### header1
		* 区块引用，注意引用后面进行换行来控制
		 
			>引用标识，同样放在行首
		* 列表，注意列表符号后面的空格		
			1. abc
			2. 345
	* 代码区块
	
			For the Chinenes, must add eight empty
			<div class="footer">
    			&copy; 2004 Foo Corporation
			</div>
			
		```
		Fenced the code
		```
	* 分隔线，三个以上的符号组成，行内不能有其他的内容
	* * *
	
	***
	
	_ _ _
			
* 区段元素
	* 链接元素 [example](http://www.google.com)，同时页支持注解方式，类似文献注解
	* 强调，_jekyll_, __jekyll__，关于斜体，加粗
	* 代码，上面已表示
	* 图片，方式类似与链接，![alt text](/img/QQ20130103-1.png)，注解方式同链接
	
* 其他元素
	* 自动链接 <http://www.google.com>,<gqlxj1987@163.com>
	* 反斜杠，用来转义
	
~~Strikethrough~~，做删除的横杠线

```
Fenced code blocks are like Standard
```	

通过3个\`符号也可以进行代码段的标识

Table

First Header | Second Header | Third Header
-------------| ------------- | ------------
Left         | Center        | Right

## [This is an example](id:anchor1)
这个代表什么?

This is the code.

    This is a peformatted






[1]: https://github.com/mojombo/jekyll "jekyll"
