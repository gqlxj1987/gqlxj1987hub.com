title: Flipkart Data Platform
date: 2018-04-02 16:59:43
categories: data platform
tags: [data platform]
---



[原文链接](https://tech.flipkart.com/overview-of-flipkart-data-platform-20c6d3e9a196)

To give some perspective on the data scale at Flipkart, FDP currently manages a 800+ nodes Hadoop cluster to store more than 35 PB of data. We also run close to 25,000 compute pipelines on our Yarn cluster. Daily TBs of data is ingested into FDP and it also handles data spikes because of sale events. The tech stack majorly comprises of HDFS, Hive, Yarn, MR, Spark, Storm & other API services supporting the meta layer of the data



Overall FDP can be broken down into following high level components.

1. Ingestion System
2. Batch Data Processing System
3. Real time Processing System
4. Report Visualization
5. Query Platform



![data-platform](https://cdn-images-1.medium.com/max/2000/0*TVaSyKyj2vrk5qvy.)



The streaming platform allows near real time aggregations to be built on all the ingested data. We also have the ability of generating rolling window aggregations i.e. 5 mins, 1 hour, 1 day, 1 month or Historic for each of the metrics.



[Apache Lens](https://lens.apache.org/) 接入hive等

![apache lens架构](https://lens.apache.org/images/arch.png)



---

## ETL

[原文链接](https://tech.flipkart.com/expressing-etl-workflows-via-cascading-192eb5e7d85d)



At the onset of Recommendation platform, we started with raw MapReduce(MR) which gave us granular control over our pipeline but required a lot of boilerplate code for performing joins and aggregations that constituted the building blocks of our ETL flow



mapreduce vs cascading

![map reduce vs cascading](https://cdn-images-1.medium.com/max/1600/0*Jl9t0eRZYu69hfcR.)

----

## REAL TIME SEACH INDEX

[原文链接](https://tech.flipkart.com/sherlock-near-real-time-search-indexing-95519783859d)



The Sherlock team developed an innovative solution (NRT data store) to deliver near real-time search results and presented it at Lucene Revolution [video](https://www.youtube.com/watch?v=05rX0mJ2N4U)



高频繁的更新操作，



![Sherlock Ingestion Overview](https://cdn-images-1.medium.com/max/1600/0*kq_TCklDIPoe_ro1.)



[sides](https://www.slideshare.net/lucidworks/near-real-time-indexing-presented-by-umesh-prasad-thejus-v-m-flipkart)

