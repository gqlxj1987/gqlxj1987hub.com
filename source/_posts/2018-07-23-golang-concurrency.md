title: Golang Concurrency
date: 2018-07-23 10:48:44
categories: golang
tags: [golang]
---



[原文链接](https://medium.com/@trevor4e/learning-gos-concurrency-through-illustrations-8c4aff603b3)



The main function indeed runs in its own go routine.



```go
<-time.After(time.Second * 5) //Receiving from channel after 5 sec
```



