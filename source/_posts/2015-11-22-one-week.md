title: 11.22一周总结
date: 2015-11-22 14:39:50
categories: weekly
tags: [weekly]
---

隔着两周没写周报，要继续保持。

本周完成：

* smart cross部分的完善
* gcm部分，针对批量发送的流程完成
* 字体脚本部分的迁移

未完成：

* gcm的流程图
* 帐号中心后续的维护
* logstash部分能否做成自动？

本周所得：

* scala相关实践
* 关于springboot以及nodejs在线上的一些实践

下周计划：

* smart cross部分的上线
* 字体脚本的一些梳理

七周七并发模型，要接着看。。。多考虑一些并发的问题

关于qps以及相关高并发的情况，多了解一些别人怎么做的。

关于scala部分，多学习，多实践！





