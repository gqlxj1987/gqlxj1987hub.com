title: Goroutine leak
date: 2018-06-11 14:24:13
categories: golang
tags: [golang]
---



[原文链接](https://medium.com/golangspec/goroutine-leak-400063aef468)



They leak if end up either blocked forever on I/O like channel communication or fall into infinite loops. Even blocked goroutine consumes resources so the program may use more memory than actually needed or eventually run out of memory and crash



### Sending to a channel without receiver





channel部分的操作



```go
import "github.com/google/gops/agent"
...
if err := agent.Start(); err != nil {
    log.Fatal(err)
}
time.Sleep(time.Hour)
> ./bin/gops
12365   gops    (/Users/mlowicki/projects/golang/spec/bin/gops)
12336*  lab     (/Users/mlowicki/projects/golang/spec/bin/lab)
> ./bin/gops vitals -p=12336
goroutines: 14
OS threads: 9
GOMAXPROCS: 4
num CPU: 4
```

通过gops的引入



runtime.Stack的方式

Stack formats a stack trace of the calling goroutine into buf and returns the number of bytes written to buf. If all is true, Stack formats stack traces of all other goroutines into buf after the trace for the current goroutine.





