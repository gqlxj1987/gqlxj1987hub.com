title: Mongodb query performance over ranges
date: 2018-02-05 14:27:58
categories: mongodb
tags: [mongodb]
---

[原文链接](https://blog.mlab.com/2012/06/cardinal-ins/?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_115)



针对`$in`操作：

* index early

* index often

* index fully

  queries make use of indexes from left to right.

* index sorts

  If your queries will contain a sort or orderby clause, add the sorted field to your index.

* commands

  ​



**The order of fields in an index should be:**

1. **First, fields on which you will query for exact values.**
2. **Second, fields on which you will sort.**
3. **Finally, fields on which you will query for a range of values.**



先对sort field部分，进行index，这样能及时保证sort之后的顺序能够得到保证