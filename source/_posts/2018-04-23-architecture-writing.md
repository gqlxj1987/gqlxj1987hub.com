title: Architecture文档说明
date: 2018-04-23 11:07:09
categories: architecture
tags: [architecture]
---



[原文链接](https://www.soasme.com/techshack.weekly/verses/fa6b9146-d4de-40ec-bded-f754ee83a5cd.html)



* User-Case View 说一些用例，覆盖到所有的组件
* Logical View 在逻辑上分层
* Process View 物理的分布，可以谈到进程，线程，节点，集群或者时下流行的Pod
* Deployment View 如何部署应用

最后是可能的安全，性能等边角问题



[模板链接](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm)



[1.   Introduction](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#introduction)     

​        [1.1 Purpose](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Purpose)
        [1.2 Scope](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Scope)
        [1.3 Definitions, Acronyms and Abbreviations](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Definitions,%20Acronyms%20and%20Abbreviations)
        [1.4 References](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#References)

[2.   Architectural Representation](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Architectural%20Representation)

[3.   Architectural Goals and Constraints](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Architectural%20Goals%20and%20Constraints)

[4.   Use-Case View](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Use-Case%20View)       

​      [4.1 Architecturally-Significant Use Cases](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Architecturally-Significant%20Use%20Cases)

[5.   Logical View](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Logical%20View)        

​     [5.1 Architecture Overview – Package and Subsystem Layering](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Architecture+Overview+%3F+Package+and+Subsystem+Layering)

[6.   Process View](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Process%20View)       

​       [6.1 Processes](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Processes)
        [6.2 Process to Design Elements](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Process%20to%20Design%20Elements)
        [6.3 Process Model to Design Model Dependencies](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Process%20Model%20to%20Design%20Model%20Dependencies)
        [6.4 Processes to the Implementation](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Processes%20to%20the%20Implementation)

[7.   Deployment View](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Deployment%20View)      

​        [7.1 External Desktop PC](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#External%20Desktop%20PC)
        [7.2 Desktop PC](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Desktop%20PC)
        [7.3 Registration Server](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Registration%20Server)
        [7.4 Course Catalog](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Course%20Catalog) 
        [7.5 Billing System ](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Billing%20System)

[8.   Size and Performance](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Size%20and%20Performance)

[9.   Quality](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch.htm#Quality)



![user case](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch_files/sadoc_3.gif)



logic层采用E-R图的方式，来进行



Process View采用类图的方式来进行

![类图](http://www.ecs.csun.edu/~rlingard/COMP684/Example2SoftArch_files/sadoc_1.gif)

