title: Scrum gets wrong
date: 2018-06-29 11:02:35
categories: scrum
tags: [scrum]
---



[原文链接](https://medium.com/@ard_adam/why-scrum-is-the-wrong-way-to-build-software-99d8994409e5)



Because all product decision authority rests with the “Product Owner”, Scrum disallows engineers from making any product decisions and reduces them to grovelling to product management for any level of inclusion in product direction.



poor design/lack of motivation



Scrum is very management heavy. Typical teams have Product Owners, Scrum Masters, and Team Leads. Innovative, motivated teams operate better with less management, not more.



可能是强调scrum磨灭了工程师自主选择的能力，无法进行相关事情的管理