title: Airbnb Architecture
date: 2018-09-18 23:47:31
categories: architecture

tags: [architecture]
---



[原文链接](https://enqueuezero.com/airbnb-architecture.html)



#### elb做load balancer



#### 服务发现

* SmartStack，对象存储，依赖zookeeper来存储发现数据
* Nerve 管理微服务的生命周期
* Synapse，服务发现，并更新haproxy
* meta信息部分，存储到zookeeper部分



#### Infrastructure

配置管理部分，采用Chef



#### 数据仓库部分

![data warehouse](https://enqueuezero.com/static/images/airbnb-architecture-data-pipeline.png)



#### microservices

后端采用[Dropwizard](https://www.dropwizard.io/1.3.5/docs/) 框架，并发布为Thrift IDL



#### search services

![search services](https://enqueuezero.com/static/images/airbnb-architecture-search.png)



Nebula is a schema-less, versioned data store service with both real-time random data access and offline batch data management

