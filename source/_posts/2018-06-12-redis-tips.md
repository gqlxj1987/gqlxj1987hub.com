title: Redis Tips
date: 2018-06-12 10:53:57
categories: redis
tags: [redis]
---



[原文链接](https://www.soasme.com/techshack.weekly/verses/062e842a-0b1e-4e4b-85a1-71be5f2c7f10.html)



* RDB

  单文件，压缩好，

  基本流程：Redis fork, 子进程把数据全量写到 rdb 文件，写完后替换老的 rdb 文件。

* AOF

  性能好，能在日志太大时自动rewrite，日志大，速度慢，

  Log rewriting 基本流程: fork, 子进程开写新的 AOF 文件，Redis 在内存中暂存数据，写完后把新的写操作应用到 AOF 文件去，最后替换老的 aof 文件



[原文链接](https://redis.io/commands/expire)



Redis keys are expired in two ways: a passive way, and an active way.

A key is passively expired simply when some client tries to access it, and the key is found to be timed out.

Of course this is not enough as there are expired keys that will never be accessed again. These keys should be expired anyway, so periodically Redis tests a few keys at random among keys with an expire set. All the keys that are already expired are deleted from the keyspace.

Specifically this is what Redis does 10 times per second:

1. Test 20 random keys from the set of keys with an associated expire.
2. Delete all the keys found expired.
3. If more than 25% of keys were expired, start again from step 1.

This is a trivial probabilistic algorithm, basically the assumption is that our sample is representative of the whole key space, and we continue to expire until the percentage of keys that are likely to be expired is under 25%

懒删除，以及定时删除，按照25%的概率进行



bgsave 对应 RDB？



hash

List(利用双端链表实现的list)

Set

Sorted Set增加score部分，进行有序排列



Redis sorted set的内部使用HashMap和跳跃表(SkipList)来保证数据的存储和有序，HashMap里放的是成员到score的映射，而跳跃表里存放的是所有的成员，排序依据是HashMap里存的score,使用跳跃表的结构可以获得比较高的查找效率，并且在实现上比较简单。

关于skipList部分

HyperLogLog

A HyperLogLog is a probabilistic data structure used in order to count unique things (technically this is referred to estimating the cardinality of a set). Usually counting unique items requires using an amount of memory proportional to the number of items you want to count, because you need to remember the elements you have already seen in the past in order to avoid counting them multiple times. However there is a set of algorithms that trade memory for precision: you end with an estimated measure with a standard error, in the case of the Redis implementation, which is less than 1%. The magic of this algorithm is that you no longer need to use an amount of memory proportional to the number of items counted, and instead can use a constant amount of memory! 12k bytes in the worst case, or a lot less if your HyperLogLog (We’ll just call them HLL from now) has seen very few elements.

