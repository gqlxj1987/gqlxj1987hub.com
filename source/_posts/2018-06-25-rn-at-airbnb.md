title: react native at airbnb
date: 2018-06-25 10:39:10
categories: react native
tags: [react native]
---



[原文链接](https://medium.com/airbnb-engineering/react-native-at-airbnb-f95aa460be1c)



Our goals with React Native were:

1. Allow us to **move faster** as an organization.
2. Maintain the **quality bar** set by native.
3. Write product code **once** for mobile instead of **twice**.
4. Improve upon the **developer experience**.



Unified Design Language System (DLS)

更改组件，导致与原本库中有所冲突，

静态检查    [prettier](https://github.com/prettier/prettier)

#### don't do well

react native immaturity

maintaining a fork of react native

javascript tooling

refactor: add proper static analysis

javascriptcore inconsistencies

initial time

Unlike with native screens, rendering React Native requires at least one full main thread -> js -> yoga layout thread -> main thread round trip before there is enough information to render a screen for the first time. As a result, we added an artificial delay of 50ms to all React Native screen transitions to prevent the navbar from flickering once the configuration was loaded.

App size

long lists

Adapter data can’t be accessed synchronously so it is possible to see views flash in as they get asynchronously rendered while scrolling quickly. Text also can’t be measured synchronously so iOS can’t make certain optimizations with pre-computed cell heights

Upgrading React Native

web同app开发的冲突



#### sunset plan

We are currently in the process of working with teams to create a healthy transition plan. We have halted all new React Native features and have plans to transition the majority of the highest-trafficked screens to native by the end of the year





server-driven rendering

Several teams have experimented with and started to unify around powerful server-driven rendering frameworks. With these frameworks, the server sends data to the device describing the components to render, the screen configuration, and the actions that can occur. Each mobile platform then interprets this data and renders native screens or even entire flows using DLS components.

MvRx

MvRx combines the best of Epoxy, [Jetpack](https://developer.android.com/jetpack/), [RxJava](https://github.com/ReactiveX/RxJava), and Kotlin with many principles from React to make building new screens easier and more seamless than ever before



