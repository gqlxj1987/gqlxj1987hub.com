title: 微服务的入门
date: 2018-03-19 19:43:28
categories: microservice
tags: [microservice]
---



[原文链接1](https://mp.weixin.qq.com/s/8Oq6TBDKe1zWwtlvkWfWPQ)

[原文链接2](http://www.infoq.com/cn/articles/micro-service-technology-stack?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_117)



文章一：

* spring boot + dubbo
* docker
* jenkins
* 关于发布服务和引用服务，如何使用



文章二：

微服务2.0：容器，PaaS，Cloud Native，gRPC，ServiceMesh，Serverless

![微服务基础架构](https://s3.amazonaws.com/infoq.content.live.0/articles/micro-service-technology-stack/zh/resources/3581-1518281425958.png)



![微服务的体系结构](https://s3.amazonaws.com/infoq.content.live.0/articles/micro-service-technology-stack/zh/resources/2852-1518281425019.png)





同时，Spring也支持Swagger契约编程模型，

Motan，gRPC

Spring Cloud体系的话，使用Eureka，同时，Consul也不错

基于nginx/openresty的Kong的服务网关

配置中心的Apollo

OpenTSDB，做Metrics的监控，Argus配合使用，

InfulxDB以及Grafana，

服务容错类型：

Hystrix，封装组件，进行容错，？(**如何使用?**)

kafka之上的，hermes加入了企业的治理能力，

针对分布式访问数据层部分，MyCAT，分表中间件，