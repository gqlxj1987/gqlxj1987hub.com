title: IPFS Intro
date: 2018-06-04 11:37:13
categories: ipfs
tags: [ipfs]
---



[原文链接](https://vinta.ws/code/ipfs-the-very-slow-distributed-permanent-web.html?utm_campaign=CodeTengu&utm_medium=email&utm_source=CodeTengu_130)



IPFS stands for InterPlanetary File System, but you could simply consider it as a distributed, permanent, but ridiculously slow, not properly functioning version of web.



ref: <https://ipfs.io/>



Every IPFS node's default storage is `10GB`, and a single node could only store data it needs, which also means each node only stores a small amount of whole data on IPFS. **If there is not enough nodes, your data might be distributed to no one except your own node.**



`ipfs add`类似git方式？hash code的方式

Pinning means storing IPFS files on local node, and prevent them from getting garbage collected. Also, you could access them much quickly. You only need to do `ipfs pin add` to pin contents someone else uploaded.



Get files? 通过hash值去get相关的文件？



IPNS stands for InterPlanetary Naming System.

Everytime you change files under a folder, the hash of the folder also changes. So you need a static reference which always points to the latest hash of your folder. You could publish your static website (a folder) to IPNS with the static reference, which is your peer ID as well as the hash of your public key.

publish a  website to ipns

After you change something, publish it again with new hash.

### Create a Domain Name Alias for Your Peer ID

可以取别名，这是优于git部分的feature？



